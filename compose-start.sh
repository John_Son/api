#!/bin/bash

# Source private ENV vars

for f in $(ls /norad/env); do
  source /norad/env/$f
done

exec bundle exec "$@"
