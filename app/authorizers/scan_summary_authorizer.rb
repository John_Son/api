# frozen_string_literal: true

class ScanSummaryAuthorizer < ApplicationAuthorizer
  def readable_by?(user, options = {})
    org = options.fetch(:in)
    reader?(user, org) || admin?(user, org)
  end
end
