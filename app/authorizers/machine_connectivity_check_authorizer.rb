# frozen_string_literal: true

class MachineConnectivityCheckAuthorizer < ApplicationAuthorizer
  class << self
    def creatable_by?(user, options)
      org = options.fetch(:in)
      admin?(user, org)
    end

    def readable_by?(user, options)
      org = options.fetch(:in)
      admin?(user, org) || reader?(user, org)
    end
  end

  def readable_by?(user)
    org = resource.machine.organization
    reader?(user, org) || admin?(user, org)
  end
end
