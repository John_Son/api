# frozen_string_literal: true

class MembershipAuthorizer < ApplicationAuthorizer
  def self.creatable_by?(user, options)
    admin?(user, options[:in])
  end

  def self.readable_by?(user, options)
    reader?(user, options[:in]) || admin?(user, options[:in])
  end

  def readable_by?(user)
    reader?(user, org) || admin?(user, org)
  end

  def deletable_by?(user)
    non_admin_resource_owner?(user) || admin_but_not_sole_admin?(user)
  end

  private

  def non_admin_resource_owner?(user)
    reader?(user, org) && resource.user == user
  end

  def admin_but_not_sole_admin?(user)
    admin?(user, org) && (resource.user != user || org.admins.count > 1)
  end

  def org
    resource.organization
  end
end
