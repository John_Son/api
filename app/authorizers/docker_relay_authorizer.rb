# frozen_string_literal: true

class DockerRelayAuthorizer < ApplicationAuthorizer
  def self.readable_by?(user, options)
    return false unless options[:in] && options[:in].is_a?(Organization)
    admin?(user, options[:in])
  end

  def readable_by?(user)
    admin?(user, resource.organization)
  end

  def updatable_by?(user)
    admin?(user, resource.organization)
  end

  def deletable_by?(user)
    admin?(user, resource.organization)
  end
end
