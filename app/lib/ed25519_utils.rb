# frozen_string_literal: true

module Ed25519Utils
  # The "openssh-key-v1" format uses this header and footer by default for ed25519 keys.
  ED25519_OPENSSH_HEADER = '-----BEGIN OPENSSH PRIVATE KEY-----'
  ED25519_OPENSSH_FOOTER = '-----END OPENSSH PRIVATE KEY-----'

  # Length of the string that identifies this key type, "ssh-ed25519" which is 11 characters
  ED25519_KEY_TYPE_STRING_LENGTH = '0000000b'

  # Used to extract the payload for computing the SHA256 fingerprint from the ed25519 public key component.
  ED25519_FINGERPRINT_PAYLOAD_BYTES = 51

  # Unfortunately, at the time of writing, no Ruby library exists to calculate the fingerprint of
  # an OpenSSH-encoded ED25519 key. This is simply the SHA256 of the public key component though.
  #
  # See: https://tools.ietf.org/html/rfc7479#page-2
  # and https://peterlyons.com/problog/2017/12/openssh-ed25519-private-key-file-format
  def compute_ed25519_fingerprint(key)
    # Some keys may already arrive decoded
    key_hex_data = decoded?(key) ? key : decode_key(key)

    # Convert to a hex string to make it easier to work with
    key_hex_bytes = key_hex_data.unpack('H*').first

    # Extract the payload from the raw key bytes
    fingerprint_payload = key_hex_bytes[payload_range(key_hex_bytes)]

    # Convert the payload back to binary representation for SHA256
    payload_bin = [fingerprint_payload].pack('H*')

    # Compute digest of binary payload and insert ":" every 2 hex digits
    OpenSSL::Digest::SHA256.hexdigest(payload_bin).scan(/../).join(':')
  end

  # Prepends and appends OpenSSH key header if not provided
  def massage_ed25519_key(key)
    key_lines = key.split("\n")
    key_lines.unshift(ED25519_OPENSSH_HEADER) unless key_lines.first == ED25519_OPENSSH_HEADER
    key_lines.push(ED25519_OPENSSH_FOOTER) unless key_lines.last == ED25519_OPENSSH_FOOTER
    Base64.strict_encode64(key_lines.join("\n"))
  end

  private

  def payload_range(key_hex_bytes)
    # The key fingerprint is the SHA256 of the following contiguous sequence in the OpenSSH key:
    # key_type_length (4 bytes) + key_type (11 bytes) + pub_key_length (4 bytes) + pub_key_payload (32 bytes)
    #
    # So once we find the starting index of the key_type_length sequence, we need to count 4+11+4+32 = 51 bytes
    # from there and compute a SHA256 over it.
    start_index = key_hex_bytes.index(ED25519_KEY_TYPE_STRING_LENGTH)

    # Don't take a chance computing the wrong signature if the payload isn't where we expect it.
    raise ArgumentError unless start_index == 86

    end_index = start_index + ED25519_FINGERPRINT_PAYLOAD_BYTES * 2 - 1

    start_index..end_index
  end

  # Returns the raw binary form of this ed25519 key
  def decode_key(key)
    Base64.strict_decode64(key.gsub(/^-.*$/, '').split("\n").join(''))
  end

  # A key may already be decoded, in which case the binary representation will start with "openssh-key-v1"
  def decoded?(decoded_key)
    decoded_key.starts_with? 'openssh-key-v1'
  end
end
