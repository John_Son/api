# frozen_string_literal: true

class DockerCommandMachineSummary
  include ActiveModel::Model

  # RBAC
  include Authority::Abilities

  # Re-use authorizer for other summaries
  self.authorizer_name = 'ScanSummaryAuthorizer'

  attr_accessor :docker_command

  def query
    DockerCommandMachineSummaryQuery.new(relation: docker_command)
  end
end
