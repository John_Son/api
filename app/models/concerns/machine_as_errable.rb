# frozen_string_literal: true

module MachineAsErrable
  extend ActiveSupport::Concern

  included do
    def machine
      errable
    end
  end
end
