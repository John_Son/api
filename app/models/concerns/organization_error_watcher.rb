# frozen_string_literal: true

module OrganizationErrorWatcher
  extend ActiveSupport::Concern

  included do
    has_many :organization_errors, as: :errable, dependent: :destroy, inverse_of: :errable

    private

    def check_for_organization_errors(error_klasses, options)
      return unless attributes_should_trigger_checks?(options[:attributes])
      # Some instances do not have an #organization method defined
      organization_method = options.fetch(:organization_method, :organization)
      subject_method = options.fetch(:subject_method, :itself)

      error_klasses.each do |klass|
        klass.send(:check, send(organization_method), subject: send(subject_method))
      end
    end

    def attributes_should_trigger_checks?(trigger_attrs)
      # Bypass for create and destroy
      return true unless transaction_include_any_action?(%i[update]) && trigger_attrs.present?
      (Array(trigger_attrs).map(&:to_s) & changed).present?
    end
  end

  module ClassMethods
    def watch_for_organization_errors(*error_klasses, **options)
      after_save(options) { check_for_organization_errors(error_klasses, options) }
      after_destroy(options) { check_for_organization_errors(error_klasses, options) }
    end

    def destroy_associated_organization_errors(*error_klasses, **options)
      subject_method = options.fetch(:subject_method, :itself)

      before_destroy(options) do
        error_klasses.each do |klass|
          klass.send(:where, errable: send(subject_method)).destroy_all
        end
      end
    end
  end
end
