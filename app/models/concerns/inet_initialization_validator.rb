# frozen_string_literal: true

module InetInitializationValidator
  extend ActiveSupport::Concern

  included do
    attr_accessor :ip_invalid

    # Rails' string_to_cidr method clobbers passed in IPs if they are invalid,
    # making it impossible to validate and add our error message, so intercept this
    # method.
    def ip=(ip_candidate)
      stripped_ip = ip_candidate&.strip
      super stripped_ip
      self.ip_invalid = ip.nil? && ip.presence != stripped_ip.presence
    end
  end
end
