# frozen_string_literal: true

# Provides helpers for mitigating timing attacks
module WithPaddedDuration
  extend ActiveSupport::Concern

  included do
    class << self
      def with_padded_duration(duration)
        start = Time.now.utc
        result = yield
      rescue StandardError => error
        sleep_remaining(start, duration, result, error)
      else
        sleep_remaining(start, duration, result)
      end

      private

      def sleep_remaining(start, duration, result, error = nil)
        finish = Time.now.utc

        # Don't sleep if the operation took longer than the
        # duration specified
        sleep [duration.to_f - (finish - start), 0].max

        raise error if error
        result
      end
    end
  end
end
