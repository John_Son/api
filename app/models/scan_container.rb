# frozen_string_literal: true

# == Schema Information
#
# Table name: scan_containers
#
#  id                    :integer          not null, primary key
#  docker_command_id     :integer          not null
#  security_container_id :integer          not null
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#
# Indexes
#
#  index_scan_containers_on_d_c_id_and_s_c_id  (docker_command_id,security_container_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_214538d76e  (security_container_id => security_containers.id) ON DELETE => cascade
#  fk_rails_a9e9cc8192  (docker_command_id => docker_commands.id) ON DELETE => cascade
#

# A join model between docker_commands and security_containers
class ScanContainer < ApplicationRecord
  belongs_to :docker_command, inverse_of: :scan_containers
  belongs_to :security_container, inverse_of: :scan_containers

  validates :docker_command, presence: true
  validates :security_container, presence: true
  validate :containers_whitelisted
  validate :relay_is_configured, if: :container_from_unofficial_repo?
  validate :foreign_keys_not_changed, on: :update

  delegate :resolved_organization, to: :docker_command

  private

  # Validations
  def relay_is_configured
    return true if resolved_organization.docker_relay_queue
    errors.add :tests, 'from unofficial repositories when no Relay configured'
    throw :abort
  end

  def containers_whitelisted
    return true if available_containers_include_security_container?
    errors.add(:tests, 'have not been whitelisted for this organization')
    throw :abort
  end

  def foreign_keys_not_changed
    return true unless docker_command_id_changed? || security_container_id_changed?
    errors.add(:base, "Can't change associated docker_command or security_container")
    throw :abort
  end

  # Helpers
  def container_from_unofficial_repo?
    !security_container.security_test_repository.official
  end

  def available_containers_include_security_container?
    return false unless security_container
    SecurityContainer.official_containers.exists?(security_container.id) ||
      resolved_organization.whitelisted_containers.exists?(security_container.id)
  end
end
