# frozen_string_literal: true

# Decorator for the Machine model
class MachineWithLatestResults < SimpleDelegator
  def initialize(machine)
    super
    @machine_results_query = MachineResultsQuery.new(Machine.where(id: machine.id))
  end

  def latest_results
    machine_results_query.for_commands(latest_command)
  end

  private

  attr_reader :machine_results_query
end
