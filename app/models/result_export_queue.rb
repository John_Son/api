# frozen_string_literal: true

# == Schema Information
#
# Table name: result_export_queues
#
#  id              :integer          not null, primary key
#  organization_id :integer          not null
#  created_by      :string           not null
#  type            :string           not null
#  auto_sync       :boolean          default(FALSE), not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_result_export_queues_on_organization_id           (organization_id)
#  index_result_export_queues_on_organization_id_and_type  (organization_id,type) UNIQUE WHERE ((type)::text = 'InfosecExportQueue'::text)
#  index_result_export_queues_on_type                      (type)
#
# Foreign Keys
#
#  fk_rails_e5fc28c9ec  (organization_id => organizations.id) ON DELETE => cascade
#

class ResultExportQueue < ApplicationRecord
  # RBAC
  include Authority::Abilities

  belongs_to :organization

  attr_readonly :type

  validates :created_by, presence: true

  def export(result_ids)
    ResultExportJob.perform_later self, result_ids
  end

  def amqp_queue_name
    'result_export_queue.norad.b10bb67d48389f217432d0240b625a37'
  end

  def target_connection_settings
    { target: 'auto' }
  end
end
