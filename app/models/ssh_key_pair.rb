# frozen_string_literal: true

# == Schema Information
#
# Table name: ssh_key_pairs
#
#  id                 :integer          not null, primary key
#  name               :string           not null
#  description        :text
#  username_encrypted :string
#  key_encrypted      :string
#  key_signature      :string           not null
#  organization_id    :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
# Indexes
#
#  index_ssh_key_pairs_on_name_and_organization_id  (name,organization_id) UNIQUE
#  index_ssh_key_pairs_on_organization_id           (organization_id)
#
# Foreign Keys
#
#  fk_rails_3811d4539d  (organization_id => organizations.id) ON DELETE => cascade
#

class SshKeyPair < ApplicationRecord
  # Vault Stuff
  include Vault::EncryptedModel
  vault_attribute :key
  vault_attribute :username

  # RBAC
  include Authority::Abilities

  # Validations
  validates :name, presence: true
  validates :name, uniqueness: { scope: :organization_id }
  validates :organization, presence: true
  validates :username, presence: true
  validates :key, presence: true
  validates_with SshKeyPairValidator

  # Associations
  belongs_to :organization, inverse_of: :ssh_key_pairs
  has_many :ssh_key_pair_assignments, inverse_of: :ssh_key_pair
  has_many :machines, through: :ssh_key_pair_assignments, inverse_of: :ssh_key_pair

  # Callbacks
  before_validation do
    # Remove newlines if the sender forgot to do so
    self.key = key.to_s.tr("\n", '')
  end

  before_create do
    decoded_key = Base64.decode64(key)
    compute_signature_and_massage_key(decoded_key)
  end

  include OrganizationErrorWatcher
  watch_for_organization_errors NoSshKeyPairError, on: %i[create destroy]

  # Destroy this key's machines' ssh connectivity errors but don't destroy
  # errors possibly created by an invalid relay ssh key
  destroy_associated_organization_errors UnableToSshToMachineError,
                                         subject_method: :machines,
                                         unless: -> { organization.configuration.use_relay_ssh_key }

  private

  include Ed25519Utils

  def compute_signature_and_massage_key(decoded_key)
    ssh_key = OpenSSL::PKey.read(decoded_key)
  rescue OpenSSL::PKey::PKeyError
    # At this point assume the key is ed25519 -- massage and compute fingerprint manually.
    self.key_signature = compute_ed25519_fingerprint(decoded_key)
    self.key = massage_ed25519_key(decoded_key)
  else
    self.key_signature = OpenSSL::Digest::SHA256.hexdigest(key_to_der(ssh_key)).scan(/../).join ':'
    self.key = Base64.strict_encode64(ssh_key.to_pem)
  end

  def key_to_der(ssh_key)
    ssh_key.public_key.to_der
  rescue NoMethodError
    # EC keys are a special case. Inspired by
    # https://github.com/ruby/openssl/issues/29#issuecomment-149799052
    point = ssh_key.public_key
    pub = OpenSSL::PKey::EC.new(point.group)
    pub.public_key = point
    pub.to_der
  end
end
