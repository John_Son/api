# frozen_string_literal: true

# == Schema Information
#
# Table name: organization_errors
#
#  id              :integer          not null, primary key
#  organization_id :integer
#  type            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  errable_type    :string
#  errable_id      :integer
#
# Indexes
#
#  index_organization_errors_on_o_id_and_type_and_e_type_and_e_id  (organization_id,errable_id,errable_type,type) UNIQUE
#
# Foreign Keys
#
#  fk_rails_823ceaeacb  (organization_id => organizations.id) ON DELETE => cascade
#

class RelayQueueError < RelayError
  validates :errable, presence: true

  # This resource is referenced in routes like so: /docker_relays/:docker_relay_id/relay_errors/:error_klass
  def to_param
    type
  end

  def message
    "Relay #{docker_relay.id} cannot connect to the AMQP queue! Check the Relay's logs for more information."
  end

  def docker_relay
    errable
  end
end
