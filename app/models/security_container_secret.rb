# frozen_string_literal: true

# == Schema Information
#
# Table name: security_container_secrets
#
#  id               :integer          not null, primary key
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  secret_encrypted :string
#

class SecurityContainerSecret < ApplicationRecord
  # Vault
  include Vault::EncryptedModel
  vault_attribute :secret

  # Adds ::expired scope
  include Perishable

  attr_readonly :secret

  has_many :assessments, inverse_of: :security_container_secret
  has_many :service_discoveries, inverse_of: :container_secret, foreign_key: :container_secret_id
  has_one :machine_connectivity_check, inverse_of: :security_container_secret

  around_create :generate_secret

  private

  def generate_secret
    self.secret = SecureRandom.hex 32
    yield
  rescue ActiveRecord::RecordNotUnique
    retry
  end
end
