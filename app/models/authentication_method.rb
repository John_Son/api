# frozen_string_literal: true

# == Schema Information
#
# Table name: authentication_methods
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  type       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_authentication_methods_on_type     (type)
#  index_authentication_methods_on_user_id  (user_id) UNIQUE
#

class AuthenticationMethod < ApplicationRecord
  # Assocations
  belongs_to :user, inverse_of: :authentication_method

  LOGIN_WHITELIST_CONFIG = Rails.root.join('config', 'new_user_whitelist.yml')

  class << self
    def local_auth?
      !reverse_proxy_auth?
    end

    def reverse_proxy_auth?
      sso_domain.present?
    end

    def find_or_create_user_for_authentication(params)
      if local_auth?
        User.confirmed_users.find_by email: params[:user]&.fetch(:email)
      elsif new_user_disabled?(params[:id])
        User.find_by(uid: params[:id])
      else
        User.find_or_create_by_uid params[:id]
      end
    end

    def whitelisted_to_login?(uid)
      return false unless File.exist?(AuthenticationMethod::LOGIN_WHITELIST_CONFIG)
      YAML.safe_load(File.read(AuthenticationMethod::LOGIN_WHITELIST_CONFIG)).include?(uid)
    end

    def new_user_disabled?(uid)
      ENV['NEW_USERS_DISABLED'] == 'true' && !whitelisted_to_login?(uid)
    end

    def sso_domain
      ENV['SSO_DOMAIN']
    end
  end
end
