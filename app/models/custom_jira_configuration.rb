# frozen_string_literal: true

# == Schema Information
#
# Table name: custom_jira_configurations
#
#  id                     :integer          not null, primary key
#  title                  :string
#  site_url               :string           not null
#  project_key            :string           not null
#  username_encrypted     :string
#  password_encrypted     :string
#  result_export_queue_id :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
# Indexes
#
#  index_custom_jira_configurations_on_result_export_queue_id  (result_export_queue_id)
#
# Foreign Keys
#
#  fk_rails_b1be8cc432  (result_export_queue_id => result_export_queues.id) ON DELETE => cascade
#

class CustomJiraConfiguration < ApplicationRecord
  # RBAC
  include Authority::Abilities

  include Vault::EncryptedModel
  vault_attribute :password
  vault_attribute :username

  belongs_to :jira_export_queue, foreign_key: :result_export_queue_id, inverse_of: :custom_jira_configuration

  # Validations
  validates :site_url,
            format: {
              with: URI.regexp(%w[http https]),
              message: 'must be a valid url'
            },
            allow_blank: false
  validates :project_key, presence: true

  # Decrypt the encrypted attributes
  def attributes
    super.tap do |a|
      a['username'] = username
      a['password'] = password
    end
  end
end
