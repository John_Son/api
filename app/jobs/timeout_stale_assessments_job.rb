# frozen_string_literal: true

class TimeoutStaleAssessmentsJob
  include ActsAsRecurringJob

  class << self
    def job_action
      Rails.logger.info 'Checking for stale Assessments...'
      stale_assessments = Assessment.stale
      stale_assessments.each(&:timeout!)
      Rails.logger.info "Stale Assessments check complete! #{stale_assessments.count} were timed out."
    end
  end
end
