# frozen_string_literal: true

class CheckForOfflineRelaysJob
  include ActsAsRecurringJob

  class << self
    def job_action
      Rails.logger.info 'Checking for offline Relays...'
      relays = DockerRelay.online.where('last_heartbeat <= ?', 1.hour.ago)
      relays.each(&:go_offline!)
      Rails.logger.info "Offline Relay check complete! #{relays.count} are now offline."
    end
  end
end
