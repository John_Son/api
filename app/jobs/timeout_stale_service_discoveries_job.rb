# frozen_string_literal: true

class TimeoutStaleServiceDiscoveriesJob
  include ActsAsRecurringJob

  class << self
    def job_action
      Rails.logger.info 'Checking for stale Service Discoveries...'
      jobs = ServiceDiscovery.stale
      jobs.each do |sd|
        sd.error_message = 'This task has not finished in at least 12 hours. Please contact Norad support.'
        sd.fail!
      end
      Rails.logger.info "Stale Service Discoveries check complete! #{jobs.count} were timed out."
    end
  end
end
