# frozen_string_literal: true

# At the time of this writing, there didn't seem to be any existing Ruby solution to validate
# OpenSSH ed25519 keys. An OpenSSH ed25519 in PEM format looks like this:
#
# -----BEGIN OPENSSH PRIVATE KEY-----
# b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAAAMwAAAAtzc2gtZW
# QyNTUxOQAAACDKY8j4vEfAGlSkpvhfkZkMCOBrExrn+AqjQUAXhAFWMAAAAKCw0lG8sNJR
# vAAAAAtzc2gtZWQyNTUxOQAAACDKY8j4vEfAGlSkpvhfkZkMCOBrExrn+AqjQUAXhAFWMA
# AAAEDh25jQkwvbYAxjBDxNAynj5/mR82aHdHWQJcmw0REYNspjyPi8R8AaVKSm+F+RmQwI
# 4GsTGuf4CqNBQBeEAVYwAAAAGGphYm91a2hlQEpBQk9VS0hFLU0tNjFEUwECAwQF
# -----END OPENSSH PRIVATE KEY-----
#
# Here, we check for 3 things for ed25519 keys
# 1. Key type is set to 'ssh-ed25519'
# 2. Key body has no illegal Base64 characters
# 3. Key is unencrypted
#
# In the future, it may be worth checking for more things, inspired by:
# - https://peterlyons.com/problog/2017/12/openssh-ed25519-private-key-file-format
# - https://cvsweb.openbsd.org/cgi-bin/cvsweb/src/usr.bin/ssh/PROTOCOL.key?annotate=HEAD
class SshKeyPairValidator < ActiveModel::Validator
  OPENSSH_UNENCRYPTED_KEY_SEQUENCE = "\x04none\x00\x00\x00\x04none"
  OPENSSH_KEY_IDENTIFIER = 'ssh-ed25519'

  def validate(record)
    @record = record
    @key_data = Base64.decode64(record.key)
    @key_lines = @key_data.split("\n")
    return add_error if key_is_obviously_invalid?
  rescue ArgumentError
    add_error
  else
    add_error unless key_is_valid_rsa_dsa_ecdsa? || key_is_valid_ed25519?
  end

  private

  def key_is_obviously_invalid?
    invalid_number_of_key_lines? || key_is_pem_encrypted?
  end

  def invalid_number_of_key_lines?
    @key_lines.empty?
  end

  def key_is_pem_encrypted?
    @key_data.match?(/ENCRYPTED/)
  end

  def key_is_valid_rsa_dsa_ecdsa?
    OpenSSL::PKey.read(@key_data)
  rescue OpenSSL::PKey::PKeyError
    false
  else
    true
  end

  def ed25519_key_bin_data(joined_key_body)
    return @key_data if @key_data.starts_with?('openssh-key-v1')
    Base64.strict_decode64(joined_key_body)
  end

  def key_is_valid_ed25519?
    joined_key_body = @key_lines[1..-2].join('')
    key_bin_data = ed25519_key_bin_data(joined_key_body)

    # OpenSSH stores ed25519 keys in the openssh-key-v1 format which means an
    # encrypted key can't be readily identified by scanning for "ENCRYPTED". Instead,
    # we must decode the packed key and ensure there is no block cipher or key derivation
    # function specified.
    #
    # An unencrypted OpenSSH ed25119 looks like this (minus the implicit newlines):
    #
    # openssh-key-v1\x00\x00\x00\x00\x04none\x00\x00\x00\x04none\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x003\x00\x00\x0
    # 0\vssh-ed25519\x00\x00\x00 \xCAc\xC8\xF8\xBCG\xC0\x1AT\xA4\xA6\xF8_\x91\x99\f\b\xE0k\x13\x1A\xE7\xF8\n\xA3A@\x17\x
    # 84\x01V0\x00\x00\x00\xA0\xB0\xD2Q\xBC\xB0\xD2Q\xBC\x00\x00\x00\vssh-ed25519\x00\x00\x00 \xCAc\xC8\xF8\xBCG\xC0\x1A
    # T\xA4\xA6\xF8_\x91\x99\f\b\xE0k\x13\x1A\xE7\xF8\n\xA3A@\x17\x84\x01V0\x00\x00\x00@\xE1\xDB\x98\xD0\x93\v\xDB`\fc\x
    # 04<M\x03)\xE3\xE7\xF9\x91\xF3f\x87tu\x90%\xC9\xB0\xD1\x11\x186\xCAc\xC8\xF8\xBCG\xC0\x1AT\xA4\xA6\xF8_\x91\x99\f\b
    # \xE0k\x13\x1A\xE7\xF8\n\xA3A@\x17\x84\x01V0\x00\x00\x00\x18jaboukhe@foobar\x01\x02\x03\x04\x05
    #
    # While an encrypted OpenSSH ed25519 using the defaults for ssh-keygen looks like this:
    #
    # openssh-key-v1\x00\x00\x00\x00\naes256-ctr\x00\x00\x00\x06bcrypt\x00\x00\x00\x18\x00\x00\x00\x10tA\xEC\xF3\xFB?\xB
    # 9\x92\xC5sb\xEE\xC0\xF3\e\xB7\x00\x00\x00\x10\x00\x00\x00\x01\x00\x00\x003\x00\x00\x00\vssh-ed25519\x00\x00\x00 \x
    # CC\x10\x18\xD1\x94\x0F#%?W\xB0\xA1x\xE6\x19\x8E7 d\xB0\xFChd`uy\x82>\xB1\xCA\vu\x00\x00\x00\xA0q\x19\xF5\\\xD9\xBE
    # \xEF<\xF5\eC\x0E\xD6N\"\x95\xD7\xE7/i`\xC1i}V\xD9\xEFS\xA9D\x9Et\x8D\xA4-\xB1[\x18\exx\x8E[Z:\x02W\xF5\xAFU\xF9\xD
    # 4\x89-\xB5\x14\xFA \xF3\x87X!$H)x\xBB`\xE6O\xB6\xBF\xC9t\xEDy6 \xA3\xADv\xCE\xA7\x82\xDEc\v\x15\n\x99\xBFI)\xD1\xA
    # B\xA7\x85\xC8 \xB9}\x82\xDF\xD1\vU\xF5A\x8B\xD1\x04l\xB3\x02\xEBS\x9DJ\xE0\xA5\x83\xDB\n6\xD2\x04*s\xFC\xFBt\x1E\x
    # A2a\xA6\xE96\x18\xD1-G%\xB9\xB7A=\xAE\x04\xAD\x9AC\x8D4\xF4@\t\xA2\x0F\xCE\xD4
    #
    key_is_openssh_encrypted = !key_bin_data.include?(OPENSSH_UNENCRYPTED_KEY_SEQUENCE)
    key_is_not_ed25519 = !key_bin_data.include?(OPENSSH_KEY_IDENTIFIER)
    raise ArgumentError if key_is_openssh_encrypted || key_is_not_ed25519
  rescue ArgumentError
    false
  else
    true
  end

  def add_error
    @record.errors.add :key, 'must be Base64 encoded and in a valid RSA/DSA/ECDSA/ED25519 format with no passphrase'
  end
end
