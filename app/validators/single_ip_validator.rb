# frozen_string_literal: true

class SingleIpValidator < ActiveModel::Validator
  def validate(record)
    @record = record
    record.errors.add(:ip, 'address must be a valid, single IPv4 address') if ip_invalid?
  end

  private

  # The inet data type will prevent anything invalid from being added to this column, but as far as
  # the data type is concered, CIDR ranges are valid values. As of now, we don't want that, so we
  # should cast the ip to a string
  def ip_invalid?
    @record.ip_invalid || (@record.ip.present? && min_ip_in_range != max_ip_in_range)
  end

  def machine_ip_range
    @record.ip.to_range
  end

  def min_ip_in_range
    machine_ip_range.min
  end

  def max_ip_in_range
    machine_ip_range.max
  end
end
