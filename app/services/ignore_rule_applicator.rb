# frozen_string_literal: true

class IgnoreRuleApplicator
  attr_reader :signature, :ignore_scope

  def initialize(signature, ignore_scope)
    @signature = signature
    @ignore_scope = ignore_scope
  end

  def apply
    results_query.ignore
  end

  def undo
    results_query.unignore
  end

  private

  def results_query
    ignore_scope.latest_results.with_signature(signature)
  end
end
