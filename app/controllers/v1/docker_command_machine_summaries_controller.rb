# frozen_string_literal: true

module V1
  class DockerCommandMachineSummariesController < ApplicationController
    before_action :load_docker_command

    # Show machine summaries for a docker command
    #
    # GET /docker_commands/{docker_command.id}/machine_summary
    def show
      summary = DockerCommandMachineSummary.new(docker_command: @docker_command)
      authorize_action_for summary, in: @docker_command.resolved_organization
      render json: summary
    end

    private

    def load_docker_command
      @docker_command = DockerCommand.find(params[:docker_command_id])
    end
  end
end
