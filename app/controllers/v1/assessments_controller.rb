# frozen_string_literal: true

module V1
  class AssessmentsController < ApplicationController
    before_action :set_assessment, only: %i[show update]
    before_action :require_container_secret_signature, only: :update
    skip_before_action :require_api_token, only: :update

    # GET /machines/:machine_id/assessments/latest
    # GET /docker_commands/:docker_command_id/assessments
    def index
      authorize_action_for Assessment, in: organization
      render json: assessments
    end

    def update
      return head :no_content if transition_state
      head :not_modified
    end

    def latest
      params[:limit] = 'latest'
      index
    end

    def show
      authorize_action_for @assessment
      render json: @assessment
    end

    private

    def organization
      return machine.organization if machine
      docker_command.resolved_organization
    end

    def docker_command
      params[:docker_command_id] && @docker_command ||= DockerCommand.find(params[:docker_command_id])
    end

    def machine
      params[:machine_id] && @machine ||= Machine.find(params[:machine_id])
    end

    def transition_state
      @assessment.complete! if update_params[:state] == 'complete'
    end

    def update_params
      params.require(:assessment).permit(:state)
    end

    def set_assessment
      @assessment = Assessment.find(params[:id])
    end

    def assessments
      return machine_scoped_assessments if machine
      docker_command_scoped_assessments
    end

    def machine_scoped_assessments
      return machine_assessments_for_docker_command if params[:docker_command_id]
      return latest_machine_assessments if limit == 'latest'
      return machine_assessments_ordered_by_created_at if limit == 'all'
      most_recent_machine_assessments_by_docker_command_id
    end

    def machine_assessments_for_docker_command
      machine.assessments.for_docker_command(params[:docker_command_id])
    end

    def latest_machine_assessments
      machine.assessments.latest
    end

    def most_recent_machine_assessments_by_docker_command_id
      machine.assessments.most_recent_by_docker_command_id
    end

    def machine_assessments_ordered_by_created_at
      machine.assessments.order(created_at: :desc)
    end

    def limit
      params[:limit].to_s
    end

    def docker_command_scoped_assessments
      Assessment.where(docker_command: docker_command)
    end

    def container_secret
      @assessment.security_container_secret&.secret
    end
  end
end
