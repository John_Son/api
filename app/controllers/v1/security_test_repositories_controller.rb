# frozen_string_literal: true

# == Schema Information
#
# Table name: security_test_repositories
#
#  id                 :integer          not null, primary key
#  host               :string
#  name               :string           not null
#  public             :boolean          default(FALSE)
#  username           :string
#  password_encrypted :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
# Indexes
#
#  index_security_test_repositories_on_host  (host)
#

module V1
  class SecurityTestRepositoriesController < ApplicationController
    before_action :set_repository, only: %i[show update destroy]
    before_action :authorize_class, only: %i[create index]

    # POST /security_test_repositories
    def create
      repository = SecurityTestRepository.new(repository_params)

      if repository.save_with_admin(current_user)
        render json: repository
      else
        render_errors_for repository
      end
    end

    # GET /security_test_repositories
    def index
      render json: SecurityTestRepository.select(:id, :name, :public, :official),
             each_serializer: SecurityTestRepositoryCollectionSerializer
    end

    # GET /security_test_repositories/:id
    def show
      render json: @repository, serializer: @repository.authorizer.serializer(current_user)
    end

    # PUT /security_test_repositories/:id
    def update
      if @repository.update(repository_params)
        render json: @repository
      else
        render_errors_for @repository
      end
    end

    # DELETE /security_test_repositories/:id
    def destroy
      if @repository.destroy
        head :no_content
      else
        render_errors_for @repository
      end
    end

    private

    def authorize_class
      authorize_action_for SecurityTestRepository
    end

    def set_repository
      @repository = SecurityTestRepository.find(params[:id])
      authorize_action_for @repository
    end

    def repository_params
      params.require(:security_test_repository).permit(:name, :username, :password, :host, :public)
    end
  end
end
