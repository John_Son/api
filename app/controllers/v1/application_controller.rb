# frozen_string_literal: true

# rubocop:disable Metrics/ClassLength
module V1
  class ApplicationController < ActionController::API
    include ActionController::Serialization
    include ActionController::HttpAuthentication::Token::ControllerMethods
    include ActiveSupport::SecurityUtils

    rescue_from(NoradApiExceptions::NotAuthorized) { head :unauthorized }
    rescue_from(NoradApiExceptions::Forbidden) { head :forbidden }

    around_action :catch_active_record_errors

    MAX_REQUEST_DELTA = 7200 # 2 hours

    before_action :require_api_token

    private

    class << self
      def allow_unauthenticated(*actions)
        skip_before_action :require_api_token, only: actions
        before_action lambda {
          raise_unauthenticated unless AuthenticationMethod.local_auth? || ui_secret_matches?
        }, only: actions
      end
    end

    def raise_unauthenticated
      raise NoradApiExceptions::NotAuthorized
    end

    def raise_forbidden
      raise NoradApiExceptions::Forbidden
    end

    def require_api_token
      t = Time.current
      authenticate_token || process_failed_login(t)
    end

    def authenticate_token
      authenticate_with_http_token do |token, _options|
        @_current_user = User.by_api_token(token)
      end
    end

    def process_failed_login(t)
      # Make all failed API token lookups take approximately the same amount of time (1s). This
      # countermeasure combined with not sending the client the X-Runtime header should provide
      # enough statistical noise to prevent timing attacks against the API token.
      sleep((1 - (Time.current - t) % 1))
      raise_unauthenticated
    end

    def current_user
      @_current_user
    end

    def render_errors_for(record)
      render json: { errors: record.errors }, status: :unprocessable_entity
    end

    # Signed request methods
    def require_pubpriv_signature(encoded_public_key)
      return head(:bad_request) unless signed_request_has_required_meta_data?
      pubkey = OpenSSL::PKey::RSA.new Base64.decode64(encoded_public_key)
      raise_unauthenticated unless signed_by_key?(pubkey)
    end

    def require_container_secret_signature(secret = container_secret)
      return on_secret_key_expired if secret.nil?
      return head(:bad_request) unless signed_request_has_required_meta_data?
      raise_unauthenticated unless signed_with_secret?(secret)
    end

    def on_secret_key_expired
      secret_errors = ActiveModel::Errors.new(SecurityContainerSecret.new)
      secret_errors.add(:base, 'Secret expired')
      render json: { errors: secret_errors }, status: :unprocessable_entity
    end

    def signed_request?
      request.headers['HTTP_NORAD_SIGNATURE'].present?
    end

    def signed_by_key?(pubkey)
      # FIXME: explore making this a global setting
      ActiveSupport.escape_html_entities_in_json = false
      valid = pubkey.verify(
        OpenSSL::Digest::SHA256.new,
        Base64.decode64(request.headers['HTTP_NORAD_SIGNATURE']),
        signed_request_params
      )
      ActiveSupport.escape_html_entities_in_json = true
      valid
    end

    def signed_with_secret?(secret)
      request.headers['HTTP_NORAD_SIGNATURE'] == compute_request_hmac(secret)
    end

    def compute_request_hmac(secret)
      # FIXME: explore making this a global setting
      ActiveSupport.escape_html_entities_in_json = false
      sig = OpenSSL::HMAC.hexdigest('sha256', secret, signed_request_params)
      ActiveSupport.escape_html_entities_in_json = true
      sig
    end

    def signed_request_has_required_meta_data?
      !timestamp_outside_of_valid_range?(params[:timestamp].to_i, Time.now.to_i) &&
        request.headers['HTTP_NORAD_SIGNATURE']
    end

    def signed_request_params
      request.request_parameters.merge(request.query_parameters).to_json
    end

    def ui_secret_matches?
      ENV['NORAD_UI_SECRET'].present? &&
        request.headers['HTTP-NORAD-UI-SECRET'].present? &&
        secure_compare(ENV['NORAD_UI_SECRET'], request.headers['HTTP-NORAD-UI-SECRET'])
    end

    def timestamp_outside_of_valid_range?(stamp, now)
      stamp < now - MAX_REQUEST_DELTA || stamp > now + MAX_REQUEST_DELTA
    end

    # Add some meta information to the Event payload
    def append_info_to_payload(payload)
      super
      payload[:user] = requestor
      payload[:remote_ip] = request.remote_ip
      payload[:request_id] = request.uuid
    end

    def requestor
      # XXX: It might be nice to have a fingerprint of the public key being used for the signed
      # request.
      signed_request? ? 'signed_request' : (current_user&.uid || 'no_valid_credentials_provided')
    end

    def catch_active_record_errors
      yield
    rescue ActiveRecord::ActiveRecordError => e
      if %w[production].include? Rails.env
        raise $ERROR_INFO, filtered_active_record_error_message(e), $ERROR_INFO.backtrace
      end
      raise $ERROR_INFO, e.message.to_s, $ERROR_INFO.backtrace
    end

    def filtered_active_record_error_message(e)
      extracted_class_name = e.message.split(' ')[0]
      "#{extracted_class_name} (FILTERED - Exception Message): (FILTERED - SQL)"
    end
  end
end
# rubocop:enable Metrics/ClassLength
