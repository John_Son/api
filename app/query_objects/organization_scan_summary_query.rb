# frozen_string_literal: true

class OrganizationScanSummaryQuery < ScanSummaryQuery
  SCAN_HISTORY_LIMIT = ORGANIZATION_SCAN_HISTORY_LIMIT

  private

  def docker_commands
    relation.docker_commands
  end
end
