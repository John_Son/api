# frozen_string_literal: true

class MachineScanSummaryQuery < ScanSummaryQuery
  SCAN_HISTORY_LIMIT = MACHINE_SCAN_HISTORY_LIMIT

  private

  def docker_commands
    relation.all_docker_commands
  end
end
