# frozen_string_literal: true

class DockerCommandMachineSummaryQuery < SummaryQuery
  def results_summary
    aggregate_summary = Hash.new { |h, k| h[k] = {} }

    # Results from the DB take the form { [machine_id, group_by_field] => count, ... , }
    # so we need to transform them to { machine_id => { group_by_field => count }, ... , }
    # so they can be efficiently aggregated into one object.
    transform_result_counts_by_machine_id!(aggregate_summary)
    transform_assessment_states_by_machine_id!(aggregate_summary)

    # Aggregate reduced summaries by machine_ids, then return only the values as an Array
    # of summary items. Each summary item contains the machine's id.
    # [
    #   {
    #     name: "machine_name",
    #     ip: "127.0.0.1",
    #     fqdn: "foobar.local",
    #     id: 42342,
    #     results_summary: { ... },
    #     assessment_states: { ... }
    #     assessments_count: 3,
    #     assessments_with_results_count: 1
    #   }
    # ]
    reduced_by_machine_attrs.each_with_object(aggregate_summary) do |machine, obj|
      build_summary_item(machine, obj[machine.id])
    end.with_indifferent_access.values
  end

  def latest_assessment_created_at
    Assessment
      .where(assessments_scope_hash)
      .select(:created_at)
      .order(created_at: :desc)
      .first
      &.created_at
  end

  private

  def build_summary_item(machine, machine_hash)
    machine_hash.merge!(machine.attributes)
    machine_hash.delete('ip')
    machine_hash.delete('fqdn')
    machine_hash[:target_address] = machine.target_address
    machine_hash[:assessments_count] = memoized_assessment_counts[machine.id].to_i
    machine_hash[:assessments_with_results_count] = memoized_assessments_with_results_counts[machine.id].to_i
  end

  def transform_result_counts_by_machine_id!(summary = {})
    reduced_by_result_counts.each_with_object(summary) do |((m_id, status), count), obj|
      obj[m_id][:results_summary] ||= DEFAULT_STATUSES.dup
      obj[m_id][:results_summary][Result::HUMAN_STATUSES[status]] = count
    end
  end

  def transform_assessment_states_by_machine_id!(summary = {})
    reduced_by_assessment_states.each_with_object(summary) do |((m_id, state), count), obj|
      obj[m_id][:assessment_states] ||= { pending_scheduling: 0, in_progress: 0, complete: 0 }
      obj[m_id][:assessment_states][state] = count
    end
  end

  def reduced_by_assessment_states
    Assessment
      .where(assessments_scope_hash)
      .group(primary_grouping_clause, :state)
      .count(:state)
  end

  def memoized_assessment_counts
    @assessment_counts ||= reduced_by_assessment_counts
  end

  def memoized_assessments_with_results_counts
    @assessments_with_results_counts ||= reduced_by_assessments_with_results_counts
  end

  def reduced_by_assessment_counts
    Machine
      .joins(:assessments)
      .where(assessments: assessments_scope_hash)
      .group(primary_grouping_clause)
      .count
  end

  def reduced_by_assessments_with_results_counts
    Machine
      .joins(assessments: :results)
      .where(assessments: assessments_scope_hash)
      .group(primary_grouping_clause)
      .distinct
      .count('assessments.id')
  end

  def reduced_by_machine_attrs
    Machine
      .select(:id, :fqdn, :use_fqdn_as_target, :ip, :name)
      .joins(:assessments)
      .where(assessments: assessments_scope_hash)
  end

  def assessments_scope_hash
    { docker_command: @relation }
  end

  def primary_grouping_clause
    'assessments.machine_id'
  end
end
