# frozen_string_literal: true

# == Schema Information
#
# Table name: application_types
#
#  id                 :integer          not null, primary key
#  port               :integer          not null
#  name               :string           not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  transport_protocol :integer          default("tcp"), not null
#
# Indexes
#
#  index_application_types_on_port  (port)
#

class ApplicationTypeSerializer < ActiveModel::Serializer
  attributes :name,
             :port,
             :transport_protocol
end
