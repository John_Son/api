# frozen_string_literal: true

# == Schema Information
#
# Table name: machines
#
#  id                 :integer          not null, primary key
#  organization_id    :integer
#  ip                 :inet
#  fqdn               :string
#  description        :text
#  machine_status     :integer          default("pending"), not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  name               :string           not null
#  use_fqdn_as_target :boolean          default(FALSE), not null
#
# Indexes
#
#  index_machines_on_name_and_organization_id  (name,organization_id) UNIQUE
#  index_machines_on_organization_id           (organization_id)
#
# Foreign Keys
#
#  fk_rails_bd87ec17a7  (organization_id => organizations.id) ON DELETE => cascade
#

class MachineShowSerializer < MachineSerializer
  attributes :enabled_tests_count

  def enabled_tests_count
    ids = SecurityContainerConfig.explicitly_enabled_for_machine_or_organization(object, object.organization)
                                 .pluck(:security_container_id)
    ids += object.organization.required_containers.pluck(:id)

    ids.uniq.count
  end
end
