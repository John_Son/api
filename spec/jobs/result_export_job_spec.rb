# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ResultExportJob, type: :job do
  it 'is in the result_export queue' do
    expect(described_class.new.queue_name).to eq('result_export')
  end

  describe 'performing the job' do
    subject(:job) { described_class.perform_later(queue, []) }
    subject(:queue) { create :infosec_export_queue }
    subject(:scan) { create :docker_command }

    after :each do
      clear_enqueued_jobs
      clear_performed_jobs
    end

    it 'exports the results' do
      exporter = double(ResultExporter)
      allow(ResultExporter).to receive(:new).with(queue, []).and_return(exporter)
      expect(exporter).to receive(:publish)
      perform_enqueued_jobs { job }
    end
  end
end
