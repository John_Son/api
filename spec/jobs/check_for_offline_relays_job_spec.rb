# frozen_string_literal: true

require 'rails_helper'
require 'jobs/shared_examples/recurring_job.rb'

RSpec.describe CheckForOfflineRelaysJob, type: :job, with_resque_doubled: true do
  before :each do
    allow(@resque_module_double).to receive(:logger)
  end

  after :each do
    clear_enqueued_jobs
    clear_performed_jobs
  end

  subject(:job) { described_class.perform }

  it_behaves_like 'a Recurring Job'

  let(:unknown_relay) { create(:online_relay) }

  it 'looks for online Relays with heartbeats older than 1 hour' do
    relay_double = class_double('DockerRelay')
    allow(DockerRelay).to receive(:online).and_return(relay_double)
    expect(relay_double).to(
      receive(:where).with('last_heartbeat <= ?', be_within(30.seconds).of(1.hour.ago)).and_return([unknown_relay])
    )
    expect(unknown_relay).to receive(:go_offline!)
    perform_enqueued_jobs { job }
  end
end
