# frozen_string_literal: true

require 'rails_helper'
require 'support/controller_helper'

RSpec.describe 'Machine Connectivity Check updates', type: :request do
  include NoradControllerTestHelpers

  it 'responds with a 422 if the job takes so long its secret is deleted' do
    connectivity_check = create :ssh_connectivity_check
    payload = {
      timestamp: Time.now.to_f.to_s,
      machine_connectivity_check: { passing: 'passing' }
    }
    sig = OpenSSL::HMAC.hexdigest(
      'sha256',
      connectivity_check.security_container_secret.secret,
      payload.to_json
    )
    connectivity_check.security_container_secret.update_attributes(secret: nil)
    signed_request :put, v1_ssh_connectivity_check_path(connectivity_check), payload, sig

    assert_response 422
    expect(response_body['errors']['base']).to eq(['Secret expired'])
  end
end
