# frozen_string_literal: true

# == Schema Information
#
# Table name: security_test_repositories
#
#  id                 :integer          not null, primary key
#  host               :string
#  name               :string           not null
#  public             :boolean          default(FALSE)
#  username           :string
#  password_encrypted :string
#  official           :boolean          default(FALSE)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
# Indexes
#
#  index_security_test_repositories_on_host  (host) UNIQUE
#  index_security_test_repositories_on_name  (name) UNIQUE
#

require 'rails_helper'

RSpec.describe SecurityTestRepository, type: :model do
  context 'when validating' do
    it { should validate_presence_of(:name) }
    it { should_not validate_presence_of(:host) }
    it { should_not validate_presence_of(:username) }
    it { should_not validate_presence_of(:password) }

    context 'if username is present' do
      before { allow(subject).to receive('username').and_return('test') }
      it { should validate_presence_of(:password) }
    end
  end

  context 'when maintaining associations' do
    it { should have_many :security_containers }
    it { should have_many(:repository_whitelist_entries).dependent(:destroy) }
  end

  context 'when cascading deletes' do
    it 'should remove containers' do
      container = create :security_container
      repo = container.security_test_repository
      expect { repo.destroy }.to change(SecurityContainer, :count).by(-1)
      expect { container.reload }.to raise_exception ActiveRecord::RecordNotFound
    end
  end

  context 'when executing callbacks' do
    before :each do
      @repository = build(:security_test_repository, username: 'test', password: 'goodpass')
    end

    it 'should encrypt password' do
      old_password = @repository.password
      @repository.save
      expect(@repository.password_encrypted).to be_present
      expect(@repository.password_encrypted).not_to include(old_password)
    end
  end

  context 'the "official" attribute' do
    it 'prevents creating records with official = true' do
      repository = build(:security_test_repository, official: true)
      expect { repository.save! }.to raise_error ActiveRecord::RecordInvalid
    end

    it 'prevents updating records with official = true' do
      repository = create :security_test_repository
      repository.update(official: true)
      expect(repository.reload.official).to eq false

      expect do
        repository.official = true
        repository.save!
      end.to raise_error ActiveRecord::RecordInvalid
    end

    it 'prevents unofficializing the official repository' do
      repository = create(:official_repository)
      repo = SecurityTestRepository.find(repository.id)
      repo.update(official: false)
      expect(repo.reload.official).to eq true
    end

    it 'allows updating when allow_official_changes = true' do
      repository = create :security_test_repository
      repository.allow_official_changes = true
      repository.official = true
      expect(repository.save).to eq true
      expect(repository.reload.official).to eq true
    end

    it 'prevents destroying records with official = true' do
      repository = create(:security_test_repository)
      allow(repository).to receive(:official).and_return(true)
      expect { repository.destroy! }.to raise_error ActiveRecord::RecordNotDestroyed
    end

    it 'allows destroying records when allow_official_changes = true' do
      repository = create(:official_repository)
      expect { repository.destroy }.to change(SecurityTestRepository, :count).by(-1)
    end
  end

  context 'the save_with_admin method' do
    before :each do
      @user = create :user
    end

    context 'for invalid security test repositories' do
      it 'does not save the role' do
        @repository = build(:private_repository, name: '')
        saved = true
        expect { saved = @repository.save_with_admin(@user) }.not_to(change(Role, :count))
        expect(saved).to eq false
        expect(@user.has_role?(:security_test_repository_admin, @repository)).to eq false
      end
    end

    context 'for valid security test repositories' do
      it 'saves the role' do
        @repository = build(:private_repository)
        expect { @repository.save_with_admin(@user) }.to change(Role, :count).by(1)
        expect(@user.has_role?(:security_test_repository_admin, @repository)).to eq true
      end
    end
  end
end
