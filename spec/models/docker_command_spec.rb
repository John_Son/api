# frozen_string_literal: true

# == Schema Information
#
# Table name: docker_commands
#
#  id                    :integer          not null, primary key
#  error_details         :text
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  machine_id            :integer
#  organization_id       :integer
#  state                 :integer          default("pending"), not null
#  assessments_in_flight :integer          default(0)
#  started_at            :datetime
#  finished_at           :datetime
#
# Indexes
#
#  index_docker_commands_on_machine_id       (machine_id)
#  index_docker_commands_on_organization_id  (organization_id)
#
# Foreign Keys
#
#  fk_rails_0545a1ce1d  (organization_id => organizations.id) ON DELETE => cascade
#  fk_rails_b85f190c31  (machine_id => machines.id) ON DELETE => cascade
#

require 'rails_helper'

RSpec.describe DockerCommand, type: :model do
  before(:each) do
    allow_any_instance_of(SshConnectivityCheck).to receive(:eligible).and_return(true)
  end

  context 'when validating' do
    it { should validate_presence_of(:scan_containers) }

    it 'requires a machine to be present if an organization is not' do
      dc = DockerCommand.new
      dc.valid?
      expect(dc.errors.messages[:machine]).to include "can't be blank"
      dc.organization = create :organization
      dc.valid?
      expect(dc.errors.messages[:machine]).to eq []
    end

    it 'requires an organization to be present if a machine is not' do
      dc = DockerCommand.new
      dc.valid?
      expect(dc.errors.messages[:organization]).to include "can't be blank"
      dc.machine = create :machine
      dc.valid?
      expect(dc.errors.messages[:organization]).to eq []
    end

    context 'RFC1918 addresses' do
      context 'when updating' do
        it 'allows updating the docker command after RFC1918 IPs are added to organization' do
          org = create :organization
          dc = create :docker_command, commandable: org
          create(:machine, ip: '10.0.0.2', organization: org)
          expect do
            dc.complete!
          end.to change(dc, :state).from('pending').to('assessments_created')
        end
      end

      context 'when creating' do
        let(:dc) { build(:docker_command, commandable: create(:organization)) }

        before do
          ENV['RFC1918_RELAY_OPTIONAL'] = 'true' if scan_env[:relay_optional]
          if scan_env[:relay_configured]
            allow_any_instance_of(Organization).to receive(:docker_relay_queue).and_return('a queue')
          end
          create(:unreachable_machine_error, organization: dc.organization) if scan_env[:unreachable_machine]
        end

        after do
          ENV.delete('RFC1918_RELAY_OPTIONAL') if scan_env[:relay_optional]
        end

        shared_examples 'it allows scanning' do
          it { expect(dc).to be_valid }
        end

        shared_examples 'it prevents scanning' do
          it do
            expect(dc).not_to be_valid
            expect(dc.errors.messages[:base]).to include(
              "Machines can't contain RFC1918 IP addresses when no Relay configured"
            )
          end
        end

        context 'when relay_optional: true, relay_configured: true, unreachable_machine: true' do
          it_behaves_like 'it allows scanning' do
            let(:scan_env) { { relay_optional: true, relay_configured: true, unreachable_machine: true } }
          end
        end

        it_behaves_like 'it allows scanning' do
          let(:scan_env) { { relay_optional: true, relay_configured: true, unreachable_machine: false } }
        end

        it_behaves_like 'it allows scanning' do
          let(:scan_env) { { relay_optional: true, relay_configured: false, unreachable_machine: true } }
        end

        it_behaves_like 'it allows scanning' do
          let(:scan_env) { { relay_optional: true, relay_configured: false, unreachable_machine: false } }
        end

        it_behaves_like 'it allows scanning' do
          let(:scan_env) { { relay_optional: false, relay_configured: true, unreachable_machine: true } }
        end

        it_behaves_like 'it allows scanning' do
          let(:scan_env) { { relay_optional: false, relay_configured: true, unreachable_machine: false } }
        end

        it_behaves_like 'it prevents scanning' do
          let(:scan_env) { { relay_optional: false, relay_configured: false, unreachable_machine: true } }
        end

        it_behaves_like 'it allows scanning' do
          let(:scan_env) { { relay_optional: false, relay_configured: false, unreachable_machine: false } }
        end
      end
    end
  end

  context 'when executing callbacks' do
    context 'before destroy' do
      let(:dc) { create :docker_command, commandable: create(:organization) }

      it 'should allow removal when scan is complete' do
        dc.complete!
        expect { dc.destroy }.to change(DockerCommand, :count).by(-1)
        expect { dc.reload }.to raise_exception ActiveRecord::RecordNotFound
      end

      it 'should allow removal when scan is canceled' do
        dc.cancel!
        expect { dc.destroy }.to change(DockerCommand, :count).by(-1)
        expect { dc.reload }.to raise_exception ActiveRecord::RecordNotFound
      end

      it 'should prevent removal when scan is pending' do
        expect(dc.pending?).to be true
        expect { dc.destroy }.not_to change(DockerCommand, :count)
      end

      it 'should prevent removal when scan is in flight' do
        allow(dc).to receive(:assessments).and_return(['an assessment'])
        dc.complete!
        expect(dc.assessments_created?).to be true
        expect(dc.finished_at).to be_blank
        expect { dc.destroy }.not_to change(DockerCommand, :count)
      end
    end
  end

  context 'when maintaining associations' do
    it { should belong_to(:machine) }
    it { should belong_to(:organization) }
    it { should have_many(:assessments) }
    it { should have_many(:scan_containers) }
    it { should have_many(:security_containers).through(:scan_containers) }
  end

  context 'when running assessments' do
    let(:machine) { build(:machine) }

    before :each do
      @started_at = 1.day.ago
      @finished_at = Time.now.utc
    end

    context 'when scanning an org' do
      let(:org) { create(:organization, machines: [create(:machine)]) }

      before :each do
        @dc = DockerCommand.new(
          scan_containers_attributes: [{ security_container_id: -1 }],
          organization: org,
          started_at: @started_at,
          finished_at: @finished_at
        )
      end

      it 'returns the correct selected machine count' do
        expect(@dc.selected_machine_count).to eq 1
      end

      it 'returns the correct scanned machine count' do
        dc = create :docker_command, commandable: org
        machine2 = create :machine, organization: org
        create :assessment, machine: machine2, docker_command: dc
        expect(dc.reload.scanned_machine_count).to eq 1
        expect(dc.reload.selected_machine_count).to eq 2
      end

      it 'increments and decrements assessments_in_flight' do
        @dc.increment :assessments_in_flight
        expect(@dc.assessments_in_flight).to eq 1
        @dc.decrement :assessments_in_flight
        expect(@dc.assessments_in_flight).to eq 0
      end
    end

    context 'when scanning a machine' do
      before :each do
        @dc = DockerCommand.new(
          scan_containers_attributes: [{ security_container_id: -1 }],
          machine: machine,
          started_at: @started_at,
          finished_at: @finished_at
        )
      end

      it 'returns the correct selected machine count' do
        expect(@dc.selected_machine_count).to eq 1
      end

      it 'increments and decrements assessments_in_flight' do
        @dc.increment :assessments_in_flight
        expect(@dc.assessments_in_flight).to eq 1
        @dc.decrement :assessments_in_flight
        expect(@dc.assessments_in_flight).to eq 0
      end
    end
  end

  describe 'instance methods' do
    describe '.decrement_assessments_in_flight' do
      it 'tells the organization to export results when scan is complete' do
        dc = build :docker_command
        allow(dc).to receive(:assessments_in_flight).and_return(0)
        allow(dc).to receive(:decrement!).with(any_args).and_return(nil)
        allow(NotificationService).to receive(:scan_complete)

        expect(dc.resolved_organization).to receive(:auto_export_results).with(dc)
        dc.decrement_assessments_in_flight
      end

      it 'instructs the Notification Service to send emails' do
        dc = build :docker_command
        allow(dc).to receive(:assessments_in_flight).and_return(0)
        allow(dc).to receive(:decrement!).with(any_args).and_return(nil)
        allow(dc.resolved_organization).to receive(:auto_export_results).with(dc)

        expect(NotificationService).to receive(:scan_complete).with(dc)
        dc.decrement_assessments_in_flight
      end
    end
  end
end
