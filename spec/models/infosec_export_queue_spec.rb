# frozen_string_literal: true

# == Schema Information
#
# Table name: result_export_queues
#
#  id              :integer          not null, primary key
#  organization_id :integer          not null
#  created_by      :string           not null
#  type            :string           not null
#  auto_sync       :boolean          default(FALSE), not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_result_export_queues_on_organization_id           (organization_id)
#  index_result_export_queues_on_organization_id_and_type  (organization_id,type) UNIQUE WHERE ((type)::text = 'InfosecExportQueue'::text)
#  index_result_export_queues_on_type                      (type)
#
# Foreign Keys
#
#  fk_rails_e5fc28c9ec  (organization_id => organizations.id) ON DELETE => cascade
#

require 'rails_helper'
require 'models/shared_examples/result_export_queue'

RSpec.describe InfosecExportQueue, type: :model do
  it_behaves_like 'a Result Export Queue' do
    subject { build(:infosec_export_queue) }
    let(:amqp_queue_name) { 'result_export_queue.cisco_infosec.21f4a6b84612b4a40d5974dcdd9e4106' }
  end

  context '#ctsm_id' do
    it 'should start with "CTSM-" and end with a number' do
      record = build(:infosec_export_queue)
      record.infosec_export_queue_configuration = build(:infosec_export_queue_configuration, ctsm_id: 'CTSM-1729')

      expect(record).to be_valid
    end

    it 'should not start with a character that is not "A"' do
      record = build(:infosec_export_queue)
      record.infosec_export_queue_configuration = build(:infosec_export_queue_configuration, ctsm_id: 'xCTSM-1729')

      expect(record).to_not be_valid
    end

    it 'should be allowed to be blank' do
      record = build(:infosec_export_queue)
      record.infosec_export_queue_configuration = build(:infosec_export_queue_configuration, ctsm_id: nil)

      expect(record).to be_valid
    end
  end

  context 'when creating duplicates' do
    it 'raises RecordNotUnique' do
      org = create :organization
      create :infosec_export_queue, organization: org
      expect { create :infosec_export_queue, organization: org }.to raise_exception ActiveRecord::RecordNotUnique
    end
  end
end
