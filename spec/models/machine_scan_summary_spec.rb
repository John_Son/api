# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MachineScanSummary, type: :model do
  let(:relation) { build_stubbed(:machine) }
  let(:instance) { described_class.new(machine: relation) }

  describe '#query' do
    it 'returns a new MachineScanSummaryQuery' do
      expect(instance.query).to be_a MachineScanSummaryQuery
    end

    it 'sets the relation for MachineScanSummaryQuery' do
      expect(instance.query.relation).to eq relation
    end
  end
end
