# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_examples 'a Result Export Queue' do
  let(:amqp_queue_name) { 'result_export_queue.norad.b10bb67d48389f217432d0240b625a37' }

  it { should belong_to(:organization) }
  it { should have_readonly_attribute(:type) }
  it { should validate_presence_of(:created_by) }

  it 'sets the proper Authority Authorizer' do
    expect(described_class.authorizer_name).to eq('ResultExportQueueAuthorizer')
  end

  describe 'instance methods' do
    describe '#export' do
      it 'schedules a ResultExport job' do
        expect(ResultExportJob).to receive(:perform_later).with(subject, [])
        subject.export([])
      end
    end

    describe '#amqp_queue_name' do
      it 'returns a string for a queue name' do
        expect(subject.amqp_queue_name).to eq(amqp_queue_name)
      end

      it 'has a hexidecimal ending' do
        expect(subject.amqp_queue_name).to match(/[a-f0-9]{32}$/)
      end

      it 'begins with result_export_queue' do
        expect(subject.amqp_queue_name).to match(/^result_export_queue\./)
      end
    end

    describe '#target_connection_settings' do
      it 'has a target key' do
        expect(subject.target_connection_settings.keys).to include(:target)
      end
    end
  end
end
