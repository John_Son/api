# frozen_string_literal: true

RSpec.shared_examples 'an Organization Error Watcher' do
  let(:instance) { create(described_class.name.underscore) }

  it 'adds the ::watch_for_organization_errors class method' do
    expect(described_class.respond_to?(:watch_for_organization_errors)).to eq(true)
  end

  it 'adds the #check_for_organization_errors instance method' do
    expect(described_class.private_method_defined?(:check_for_organization_errors)).to eq(true)
  end

  context 'conditionals' do
    it 'when false' do
      klass = Class.new { def self.check(*); end }
      described_class.class_eval { watch_for_organization_errors klass, organization_method: :itself, if: -> { false } }
      expect(klass).not_to receive(:check)
      instance.destroy
    end

    it 'when true' do
      klass = Class.new { def self.check(*); end }
      allow(klass).to receive(:check)
      described_class.class_eval { watch_for_organization_errors klass, organization_method: :itself, if: -> { true } }
      expect(klass).to receive(:check)
      instance.destroy
    end
  end

  it 'calls ::check by default' do
    klass = Class.new { def self.check(*); end }
    allow(klass).to receive(:check)
    described_class.class_eval { watch_for_organization_errors klass, organization_method: :itself }
    expect(klass).to receive(:check)
    instance.destroy
  end
end
