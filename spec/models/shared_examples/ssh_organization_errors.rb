# frozen_string_literal: true

require 'support/organization_error_spec_helpers'

RSpec.shared_examples 'SSH Organization Error class' do
  include OrganizationErrorSpecHelpers

  let(:org) { error.organization }
  let(:machine) { error.machine }

  it 'removes error when neither ssh service nor key pair assignment exist' do
    expect(described_errors(org)).not_to be_empty
    described_class.check(org, subject: machine)
    expect(described_errors(org)).to be_empty
  end

  it 'removes error when both ssh service and key pair assignment exist' do
    expect(described_errors(org)).not_to be_empty
    create :ssh_service, machine: machine
    create :ssh_key_pair_assignment, machine: machine
    described_class.check(org, subject: machine.reload)
    expect(described_errors(org)).to be_empty
  end
end
