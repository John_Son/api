# frozen_string_literal: true

require 'rails_helper'

describe ResultExporterAuthorizer, type: :authorizer do
  before :each do
    org1 = create :organization
    @admin_user = create :user
    @admin_user.add_role :organization_admin, org1
    @reader_user = create :user
    @reader_user.add_role :organization_reader, org1
    @options = { in: org1 }

    org2 = create :organization
    @admin_user_other = create :user
    @admin_user_other.add_role :organization_admin, org2
    @reader_user_other = create :user
    @reader_user_other.add_role :organization_reader, org2
  end

  it 'allows org admins and readers to create' do
    expect(ResultExporter.authorizer).to be_creatable_by(@admin_user, @options)
    expect(ResultExporter.authorizer).to be_creatable_by(@reader_user, @options)
  end

  it 'does not allow admins and readers from a different organization to create' do
    expect(ResultExporter.authorizer).to_not be_creatable_by(@admin_user_other, @options)
    expect(ResultExporter.authorizer).to_not be_creatable_by(@reader_user_other, @options)
  end
end
