# frozen_string_literal: true

require 'rails_helper'

describe MembershipAuthorizer, type: :authorizer do
  before :each do
    @my_org = create :organization
    their_org = create :organization
    @admin_user = create :user
    @reader_user = create :user
    @other_admin_user = create :user
    @other_reader_user = create :user
    @admin_user.add_role :organization_admin, @my_org
    @reader_user.add_role :organization_reader, @my_org
    @other_admin_user.add_role :organization_admin, their_org
    @other_reader_user.add_role :organization_reader, their_org
  end

  context 'at the instance level' do
    before :each do
      @membership = create :membership, organization_id: @my_org.id, user_id: @admin_user.id
    end

    context 'for read actions' do
      it 'permit admins in org' do
        expect(@membership.authorizer).to be_readable_by(@admin_user)
      end

      it 'permit readers in org' do
        expect(@membership.authorizer).to be_readable_by(@reader_user)
      end

      it 'do not permit admins in other org' do
        expect(@membership.authorizer).not_to be_readable_by(@other_admin_user)
      end

      it 'do not permit readers in other org' do
        expect(@membership.authorizer).not_to be_readable_by(@other_reader_user)
      end
    end

    context 'for destroy actions' do
      context 'when an admin is not the sole admin user for their organization' do
        before(:each) do
          @other_admin_user.add_role :organization_admin, @my_org
        end

        it 'permits deletion of oneself from their organization' do
          expect(@membership.authorizer).to be_deletable_by(@admin_user)
        end
      end

      context 'when an admin is the sole admin user for their organization' do
        before(:each) do
          expect(@my_org.admins.pluck(:id)).to eq([@admin_user.id])
        end

        it 'does not permit deletion of oneself from their organization' do
          expect(@membership.authorizer).to_not be_deletable_by(@admin_user)
        end
      end

      context 'when membership is a reader' do
        before(:each) do
          @reader_membership = create :membership, organization_id: @my_org.id, user_id: @reader_user.id
        end

        it 'permits an admin user to delete a reader user' do
          expect(@reader_membership.authorizer).to be_deletable_by(@admin_user)
        end

        it 'permits readers to delete themselves from their organization' do
          expect(@reader_membership.authorizer).to be_deletable_by(@reader_user)
        end

        it 'does not permit readers to delete readers in the same org' do
          same_org_reader_user = create :user
          same_org_reader_user.add_role :organization_reader, @my_org

          expect(@reader_membership.authorizer).not_to be_deletable_by(same_org_reader_user)
        end
      end

      it 'does not permit readers to delete admins in the same org' do
        expect(@membership.authorizer).not_to be_deletable_by(@reader_user)
      end

      it 'do not permit admin in other org' do
        expect(@membership.authorizer).not_to be_deletable_by(@other_admin_user)
      end

      it 'do not permit reader in other org' do
        expect(@membership.authorizer).not_to be_deletable_by(@other_reader_user)
      end
    end
  end

  context 'at the class level' do
    before :each do
      @options = { in: @my_org }
    end

    context 'for read actions' do
      it 'permit admins' do
        expect(Membership.authorizer).to be_readable_by(@admin_user, @options)
      end

      it 'do not permit readers' do
        expect(Membership.authorizer).to be_readable_by(@reader_user, @options)
      end

      it 'do not permit admin from other org' do
        expect(Membership.authorizer).not_to be_readable_by(@other_admin_user, @options)
      end

      it 'do not permit reader from other org' do
        expect(Membership.authorizer).not_to be_readable_by(@other_reader_user, @options)
      end
    end

    context 'for create actions' do
      it 'permit admins' do
        expect(Membership.authorizer).to be_creatable_by(@admin_user, @options)
      end

      it 'do not permit readers' do
        expect(Membership.authorizer).not_to be_creatable_by(@reader_user, @options)
      end

      it 'do not permit admin from other org' do
        expect(Membership.authorizer).not_to be_creatable_by(@other_admin_user, @options)
      end

      it 'do not permit reader from other org' do
        expect(Membership.authorizer).not_to be_creatable_by(@other_reader_user, @options)
      end
    end
  end
end
