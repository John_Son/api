# frozen_string_literal: true

require 'rails_helper'
require 'authorizers/shared_examples/result_export_queue_authorizer'

describe 'JiraExportQueueAuthorizer', type: :authorizer do
  let(:authorizing_klass) { JiraExportQueue }
  it_behaves_like 'a Result Export Queue Authorizer'
end
