# frozen_string_literal: true

require 'rails_helper'

RSpec.describe V1::ScansController, type: :controller do
  context 'POST #create' do
    before :each do
      @rg = create :requirement_group
      @req = build :requirement
      @sec_con = create :security_container
      @rg.requirements << @req
      @req.security_containers << @sec_con
    end

    context 'as an organization admin' do
      context 'for organizations' do
        before :each do
          @org = create :organization
          @_current_user.add_role :organization_admin, @org
        end

        it 'creates nothing if no containers are enabled or required' do
          expect { norad_post :create, organization_id: @org.to_param }.to change(DockerCommand, :count).by(0)
        end

        it 'creates a Docker Command with a list of required containers' do
          @org.requirement_groups << @rg
          expect { norad_post :create, organization_id: @org.to_param }.to change(DockerCommand, :count).by(1)
          dc = @org.docker_commands.last
          expect(dc.security_containers).to match_array([@sec_con])
        end

        it 'creates a Docker Command with a list of explicitly enabled containers' do
          config = create :security_container_config, enabled_outside_of_requirement: true, configurable: @org
          other_org = create :organization
          create :security_container_config, enabled_outside_of_requirement: true, configurable: other_org
          expect { norad_post :create, organization_id: @org.to_param }.to change(DockerCommand, :count).by(1)
          dc = @org.docker_commands.last
          expect(dc.security_containers).to match_array([config.security_container])
        end

        it 'creates a Docker Command with a list of explicitly enabled containers and required_containers' do
          @org.requirement_groups << @rg
          config = create :security_container_config, enabled_outside_of_requirement: true, configurable: @org
          expect { norad_post :create, organization_id: @org.to_param }.to change(DockerCommand, :count).by(1)
          dc = @org.docker_commands.last
          expect(dc.security_containers).to match_array([config.security_container, @sec_con])
        end
      end

      context 'for machines' do
        before :each do
          @machine = create :machine
          @org = @machine.organization
          @_current_user.add_role :organization_admin, @org
        end

        it 'creates nothing if no containers are enabled or required' do
          expect { norad_post :create, machine_id: @machine.to_param }.to change(DockerCommand, :count).by(0)
        end

        it 'creates a Docker Command with a list of required containers' do
          @org.requirement_groups << @rg
          expect { norad_post :create, machine_id: @machine.to_param }.to change(DockerCommand, :count).by(1)
          dc = @machine.docker_commands.last
          expect(dc.security_containers).to match_array([@sec_con])
        end

        it 'creates a Docker Command with a list of explicitly enabled containers' do
          config1 = create :security_container_config, enabled_outside_of_requirement: true, configurable: @machine
          org = @machine.organization
          config2 = create :security_container_config, enabled_outside_of_requirement: true, configurable: org
          other_machine = create :machine
          create :security_container_config, enabled_outside_of_requirement: true, configurable: other_machine
          expect { norad_post :create, machine_id: @machine.to_param }.to change(DockerCommand, :count).by(1)
          dc = @machine.docker_commands.last
          expect(dc.security_containers).to match_array([config1.security_container, config2.security_container])
        end

        it 'creates a Docker Command with a list of explicitly enabled containers and required_containers' do
          @org.requirement_groups << @rg
          config = create :security_container_config, enabled_outside_of_requirement: true, configurable: @org
          expect { norad_post :create, machine_id: @machine.to_param }.to change(DockerCommand, :count).by(1)
          dc = @machine.docker_commands.last
          expect(dc.security_containers).to match_array([config.security_container, @sec_con])
        end
      end
    end

    context 'as an organization reader' do
      context 'for organizations' do
        before :each do
          @org = create :organization
          @_current_user.add_role :organization_reader, @org
        end

        it 'creates nothing if no containers are enabled or required' do
          expect { norad_post :create, organization_id: @org.to_param }.to change(DockerCommand, :count).by(0)
          expect(response.status).to eq(403)
        end

        it 'creates a Docker Command with a list of required containers' do
          @org.requirement_groups << @rg
          expect { norad_post :create, organization_id: @org.to_param }.to change(DockerCommand, :count).by(0)
          expect(response.status).to eq(403)
        end

        it 'creates a Docker Command with a list of explicitly enabled containers' do
          create :security_container_config, enabled_outside_of_requirement: true, configurable: @org
          expect { norad_post :create, organization_id: @org.to_param }.to change(DockerCommand, :count).by(0)
          expect(response.status).to eq(403)
        end

        it 'creates a Docker Command with a list of explicitly enabled containers and required_containers' do
          @org.requirement_groups << @rg
          create :security_container_config, enabled_outside_of_requirement: true, configurable: @org
          expect { norad_post :create, organization_id: @org.to_param }.to change(DockerCommand, :count).by(0)
          expect(response.status).to eq(403)
        end
      end

      context 'for machines' do
        before :each do
          @machine = create :machine
          @org = @machine.organization
          @_current_user.add_role :organization_reader, @org
        end

        it 'creates nothing if no containers are enabled or required' do
          expect { norad_post :create, machine_id: @machine.to_param }.to change(DockerCommand, :count).by(0)
          expect(response.status).to eq(403)
        end

        it 'creates a Docker Command with a list of required containers' do
          @org.requirement_groups << @rg
          expect { norad_post :create, machine_id: @machine.to_param }.to change(DockerCommand, :count).by(0)
          expect(response.status).to eq(403)
        end

        it 'creates a Docker Command with a list of explicitly enabled containers' do
          create :security_container_config, enabled_outside_of_requirement: true, configurable: @machine
          expect { norad_post :create, machine_id: @machine.to_param }.to change(DockerCommand, :count).by(0)
          expect(response.status).to eq(403)
        end

        it 'creates a Docker Command with a list of explicitly enabled containers and required_containers' do
          @org.requirement_groups << @rg
          create :security_container_config, enabled_outside_of_requirement: true, configurable: @org
          expect { norad_post :create, machine_id: @machine.to_param }.to change(DockerCommand, :count).by(0)
          expect(response.status).to eq(403)
        end
      end
    end

    context 'as a user with no role' do
      context 'for organizations' do
        before :each do
          @org = create :organization
        end

        it 'creates nothing if no containers are enabled or required' do
          expect { norad_post :create, organization_id: @org.to_param }.to change(DockerCommand, :count).by(0)
          expect(response.status).to eq(403)
        end

        it 'creates a Docker Command with a list of required containers' do
          @org.requirement_groups << @rg
          expect { norad_post :create, organization_id: @org.to_param }.to change(DockerCommand, :count).by(0)
          expect(response.status).to eq(403)
        end

        it 'creates a Docker Command with a list of explicitly enabled containers' do
          create :security_container_config, enabled_outside_of_requirement: true, configurable: @org
          expect { norad_post :create, organization_id: @org.to_param }.to change(DockerCommand, :count).by(0)
          expect(response.status).to eq(403)
        end

        it 'creates a Docker Command with a list of explicitly enabled containers and required_containers' do
          @org.requirement_groups << @rg
          create :security_container_config, enabled_outside_of_requirement: true, configurable: @org
          expect { norad_post :create, organization_id: @org.to_param }.to change(DockerCommand, :count).by(0)
          expect(response.status).to eq(403)
        end
      end

      context 'for machines' do
        before :each do
          @machine = create :machine
          @org = @machine.organization
        end

        it 'creates nothing if no containers are enabled or required' do
          expect { norad_post :create, machine_id: @machine.to_param }.to change(DockerCommand, :count).by(0)
          expect(response.status).to eq(403)
        end

        it 'creates a Docker Command with a list of required containers' do
          @org.requirement_groups << @rg
          expect { norad_post :create, machine_id: @machine.to_param }.to change(DockerCommand, :count).by(0)
          expect(response.status).to eq(403)
        end

        it 'creates a Docker Command with a list of explicitly enabled containers' do
          create :security_container_config, enabled_outside_of_requirement: true, configurable: @machine
          expect { norad_post :create, machine_id: @machine.to_param }.to change(DockerCommand, :count).by(0)
          expect(response.status).to eq(403)
        end

        it 'creates a Docker Command with a list of explicitly enabled containers and required_containers' do
          @org.requirement_groups << @rg
          create :security_container_config, enabled_outside_of_requirement: true, configurable: @org
          expect { norad_post :create, machine_id: @machine.to_param }.to change(DockerCommand, :count).by(0)
          expect(response.status).to eq(403)
        end
      end
    end
  end
end
