# frozen_string_literal: true

require 'rails_helper'

RSpec.describe V1::PasswordResetsController, type: :controller do
  before(:each) do
    mock_auth_type :local
    @user = create(:user_with_password).reload
    @user.confirm!
  end

  after(:each) { mock_auth_type :reverse_proxy }

  describe 'GET #show' do
    let(:token_params) { { provided_token: @user.local_authentication_record.password_reset_token } }

    before :each do
      @user.reset_password!
    end

    it 'responds with password reset for valid token' do
      norad_get :show, token_params
      expect(response.status).to be 200
    end

    it 'raises NotFound for an expired token' do
      @user.local_authentication_record.update!(password_reset_sent_at: 3.days.ago)
      expect { norad_get :show, token_params }.to raise_exception ActiveRecord::RecordNotFound
    end

    it 'raises NotFound for a missing token' do
      expect { norad_get :show, provided_token: 'blah' }.to raise_exception ActiveRecord::RecordNotFound
    end
  end

  # expect change() syntax is broken here because we need a reload after calling reset_password!
  describe 'POST #create' do
    it 'responds with success for valid email' do
      old_token = @user.local_authentication_record.password_reset_token
      norad_post :create, email: @user.email
      expect(old_token).not_to eq(@user.local_authentication_record.reload.password_reset_token)
      expect(response.status).to be 204
    end

    it 'responds with success for invalid email' do
      old_token = @user.local_authentication_record.password_reset_token
      norad_post :create, email: 'invalid@norad.dev'
      expect(old_token).to eq(@user.local_authentication_record.reload.password_reset_token)
      expect(response.status).to be 204
    end
  end

  describe 'PUT #update' do
    let(:valid_new_password_params) do
      {
        provided_token: @user.local_authentication_record.password_reset_token,
        password: 'newpassword',
        password_confirmation: 'newpassword'
      }
    end
    let(:mismatched_new_password_params) do
      {
        provided_token: @user.local_authentication_record.password_reset_token,
        password: 'newpassword',
        password_confirmation: 'password'
      }
    end
    let(:invalid_new_password_params) do
      {
        provided_token: 'blah',
        password: 'newpassword',
        password_confirmation: 'newpassword'
      }
    end

    def new_digest
      @user.local_authentication_record.reload.password_digest
    end

    before(:each) { @old_digest = @user.local_authentication_record.password_digest }

    context 'when a user has requested it' do
      before(:each) { @user.reset_password! }

      it "updates the user's password with valid token" do
        expect(@old_digest).to eq new_digest

        norad_put :update, valid_new_password_params
        expect(response.status).to eq 204
        expect(@old_digest).not_to eq new_digest
      end

      it "doesn't update the user's password with invalid token" do
        expect { norad_put :update, invalid_new_password_params }.to raise_exception ActiveRecord::RecordNotFound
        expect(@old_digest).to eq new_digest
      end

      it "doesn't update the user's password with expired token" do
        @user.local_authentication_record.update(password_reset_sent_at: 3.days.ago)
        expect { norad_put :update, valid_new_password_params }.to raise_exception ActiveRecord::RecordNotFound
        expect(@old_digest).to eq new_digest
      end

      it "doesn't update the user's password when the confirmation doesn't match" do
        norad_put :update, mismatched_new_password_params
        expect(@old_digest).to eq new_digest
        expect(response.status).to eq 422
        expect(JSON.parse(response.body)['errors']['password_confirmation'].first).to eq "doesn't match Password"
      end
    end

    context "when a user hasn't requested it" do
      it "doesn't update the user's password even with valid token" do
        expect { norad_put :update, valid_new_password_params }.to raise_exception ActiveRecord::RecordNotFound
        expect(@old_digest).to eq new_digest
      end
    end
  end
end
