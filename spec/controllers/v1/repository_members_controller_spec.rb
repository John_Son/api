# frozen_string_literal: true

require 'rails_helper'

RSpec.describe V1::RepositoryMembersController, type: :controller do
  let(:repository) { create :private_repository }

  describe 'GET #index' do
    before :each do
      @readers = Array.new(5) { create :repository_reader, security_test_repository: repository }
      @admin = create :repository_admin, user: @_current_user, security_test_repository: repository
    end

    it 'retrieves list of members' do
      norad_get :index, security_test_repository_id: repository.to_param
      expect(response).to have_http_status :success
      expect(response).to match_response_schema('repository_members')
      expect(response_body['response'].map { |m| m['id'] }).to contain_exactly(*(@readers.map(&:id) << @admin.id))
    end
  end

  describe 'POST #create' do
    before :each do
      create :repository_admin, security_test_repository: repository, user: @_current_user
    end

    it 'creates admin' do
      expect do
        other_user = create :user
        norad_post :create,
                   security_test_repository_id: repository.to_param,
                   repository_member: { uid: other_user.to_param, role_type: :admin }
      end.to change(RepositoryMember, :count).by(1)
      expect(response).to have_http_status :success
      expect(response).to match_response_schema('repository_member')
    end

    it 'renders errors' do
      expect do
        norad_post :create,
                   security_test_repository_id: repository.to_param,
                   repository_member: { uid: @_current_user.to_param, role_type: :admin }
      end.not_to change(RepositoryMember, :count)
      expect(response).to have_http_status :unprocessable_entity
    end
  end

  describe 'DELETE #destroy' do
    it 'removes reader' do
      reader = create :repository_reader
      create :repository_admin, user: @_current_user, security_test_repository: reader.security_test_repository
      expect { norad_delete :destroy, id: reader.to_param }.to change(RepositoryMember, :count).by(-1)
      expect(response).to have_http_status :no_content
    end

    it 'does not remove current_user' do
      admin = create :repository_admin, user: @_current_user
      expect { norad_delete :destroy, id: admin.to_param }.not_to change(RepositoryMember, :count)
      expect(response).to have_http_status :forbidden
      expect(@_current_user.has_role?(:security_test_repository_admin, admin.security_test_repository)).to be_truthy
    end
  end
end
