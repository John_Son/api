# frozen_string_literal: true

require 'rails_helper'

RSpec.describe V1::MembershipsController, type: :controller do
  before :each do
    @org = create :organization
  end

  describe 'GET #index' do
    before :each do
      @new_user1 = create :user
      @new_user2 = create :user
    end

    it 'exposes memberships for your organization as reader' do
      Membership.create_reader!(@_current_user, @org)
      Membership.create_reader!(@new_user1, @org)
      Membership.create_reader!(@new_user2, @org)
      norad_get :index, organization_id: @org.to_param
      expect(response.status).to eq(200)
      expect(response).to match_response_schema('memberships')
    end

    it 'exposes memberships for your organization as admin' do
      Membership.create_admin!(@_current_user, @org)
      Membership.create_reader!(@new_user1, @org)
      Membership.create_reader!(@new_user2, @org)
      norad_get :index, organization_id: @org.to_param
      expect(response.status).to eq(200)
      expect(response).to match_response_schema('memberships')
    end

    it 'as reader does not expose memberships for other organizations' do
      other_org = create :organization
      @_current_user.add_role :organization_reader, @org
      @new_user1.add_role :organization_reader, other_org
      @new_user2.add_role :organization_reader, other_org
      norad_get :index, organization_id: other_org.to_param
      expect(response.status).to eq(403)
    end

    it 'as admin does not expose memberships for other organizations' do
      other_org = create :organization
      @_current_user.add_role :organization_admin, @org
      @new_user1.add_role :organization_reader, other_org
      @new_user2.add_role :organization_reader, other_org
      norad_get :index, organization_id: other_org.to_param
      expect(response.status).to eq(403)
    end
  end

  describe 'POST #create' do
    def create_membership(org, new_user, admin)
      norad_post :create, organization_id: org.to_param, membership: { admin: admin.to_s, uid: new_user.uid }
    end

    it 'allows admins to add a new admin user' do
      @_current_user.add_role :organization_admin, @org
      new_user = create :user
      expect { create_membership(@org, new_user, true) }.to change(Membership, :count).by(1)
      expect(response.status).to eq(200)
      # INFO: Had a bug where the roles where not being created for the desired org. Checks
      # that the correct membership role type is created, and on the correct organization.
      role = new_user.roles.find_by(resource_type: 'Organization', resource_id: @org.id)
      expect(role).to_not be(nil)
      expect(role.name).to eq('organization_admin')
    end

    it 'does not allow duplicate admin users' do
      @_current_user.add_role :organization_admin, @org
      new_user = create :user

      expect { create_membership(@org, new_user, true) }.to change(Membership, :count).by(1)
      expect { create_membership(@org, new_user, true) }.to change(Membership, :count).by(0)

      assert_response 422
      expect(response_body['errors']['user']).to eq(['has already been taken'])
    end

    it 'allows admins to add a new reader user' do
      @_current_user.add_role :organization_admin, @org
      new_user = create :user
      expect { create_membership(@org, new_user, false) }.to change(Membership, :count).by(1)
      expect(response.status).to eq(200)
      # INFO: Had a bug where the roles where not being created for the desired org. Checks
      # that the correct membership role type is created, and on the correct organization.
      role = new_user.roles.find_by(resource_type: 'Organization', resource_id: @org.id)
      expect(role).to_not be(nil)
      expect(role.name).to eq('organization_reader')
    end

    it 'does not allow readers to add a new admin user' do
      @_current_user.add_role :organization_reader, @org
      new_user = create :user
      expect { create_membership(@org, new_user, true) }.to change(Membership, :count).by(0)
      expect(response.status).to eq(403)
    end

    it 'does not allow readers to add a new reader user' do
      @_current_user.add_role :organization_reader, @org
      new_user = create :user
      expect { create_membership(@org, new_user, false) }.to change(Membership, :count).by(0)
      expect(response.status).to eq(403)
    end
  end

  describe 'DELETE #destroy' do
    before :each do
      @admin_user = create :user
      @admin_user.add_role :organization_admin, @org
      @admin_membership = create :membership, organization: @org, user: @admin_user

      @reader_user = create :user
      @reader_user.add_role :organization_reader, @org
      @reader_membership = create :membership, organization: @org, user: @reader_user
    end

    def delete_membership(membership)
      norad_delete :destroy, id: membership.id
    end

    it 'allows admins to remove an admin user' do
      role_count = @admin_membership.user.roles.length
      @_current_user.add_role :organization_admin, @org
      expect { delete_membership(@admin_membership) }.to change(Membership, :count).by(-1)
      @admin_user.reload
      expect(role_count - 1).to eq(@admin_membership.user.roles.length)
      expect(response.status).to eq(204)
    end

    it 'allows admins to remove a reader user' do
      role_count = @reader_membership.user.roles.length
      @_current_user.add_role :organization_admin, @org
      expect { delete_membership(@reader_membership) }.to change(Membership, :count).by(-1)
      @reader_user.reload
      expect(role_count - 1).to eq(@reader_membership.user.roles.length)
      expect(response.status).to eq(204)
    end

    it 'does not allow readers to remove an admin user' do
      @_current_user.add_role :organization_reader, @org
      expect { delete_membership(@admin_membership) }.to change(Membership, :count).by(0)
      expect(response.status).to eq(403)
    end

    it 'does not allow readers to remove a reader user' do
      @_current_user.add_role :organization_reader, @org
      user = create :user
      user.add_role :organization_reader, @org
      expect { delete_membership(@reader_membership) }.to change(Membership, :count).by(0)
      expect(response.status).to eq(403)
    end

    context 'for a user with multiple roles in an org' do
      before :each do
        @reader_user.add_role :organization_admin, @org
      end

      it 'remove all roles' do
        role_count = @reader_membership.user.roles.length
        @_current_user.add_role :organization_admin, @org
        expect { delete_membership(@reader_membership) }.to change(Membership, :count).by(-1)
        @reader_user.reload
        expect(role_count - 2).to eq(@reader_membership.user.roles.length)
        expect(response.status).to eq(204)
      end
    end

    context 'make sure at least one admin is left in an org' do
      before :each do
        @user2 = create :user
        @user3 = create :user
        new_org = create :organization
        @_current_user.add_role :organization_admin, new_org
        @user2.add_role :organization_admin, new_org
        @user3.add_role :organization_admin, new_org
        @admin_membership1 = create :membership, organization: new_org, user: @_current_user
        @admin_membership2 = create :membership, organization: new_org, user: @user2
        @admin_membership3 = create :membership, organization: new_org, user: @user3
      end

      it 'try to delete all the admins and fail' do
        role_count2 = @admin_membership2.user.roles.length
        role_count3 = @admin_membership3.user.roles.length
        expect { delete_membership(@admin_membership3) }.to change(Membership, :count).by(-1)
        @user3.reload
        expect(role_count3 - 1).to eq(@admin_membership3.user.roles.length)
        expect { delete_membership(@admin_membership2) }.to change(Membership, :count).by(-1)
        @user2.reload
        expect(role_count2 - 1).to eq(@admin_membership2.user.roles.length)
      end

      context 'when an admin is the sole admin user for their organization' do
        before(:each) do
          @admin_membership2.destroy_with_roles!
          @admin_membership3.destroy_with_roles!
          expect(@admin_membership1.organization.admins.pluck(:id)).to eq([@admin_membership1.user.id])
        end

        it 'does not permit deletion of oneself from their organization' do
          # Try to remove yourself.
          role_count1 = @admin_membership1.user.roles.length
          expect { delete_membership(@admin_membership1) }.to change(Membership, :count).by(0)
          @_current_user.reload
          expect(role_count1).to eq(@admin_membership1.user.roles.length)
        end
      end
    end
  end
end
