# frozen_string_literal: true

require 'rails_helper'
require 'controllers/v1/shared_examples/connectivity_checks_controller'

RSpec.describe V1::SshConnectivityChecksController, type: :controller do
  it_should_behave_like 'a connectivity checks controller' do
    let(:factory) { :ssh_connectivity_check }
  end
end
