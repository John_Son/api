# frozen_string_literal: true

require 'rails_helper'

RSpec.describe DockerCommandDecorator do
  describe '#state_details' do
    it 'shows the error details if present' do
      dc = build :docker_command, error_details: 'Test error details'
      expect(described_class.new(dc).state_details).to include('Test error details')
    end

    it 'shows "No assessments created"' do
      dc = create :docker_command
      dc.complete!
      expect(described_class.new(dc).state_details).to include('No relevant assessments')
    end

    it 'shows "Scan started successfully"' do
      dc = create :docker_command
      create :white_box_assessment, machine: dc.machine, docker_command: dc
      dc.complete!
      expect(described_class.new(dc).state_details).to include('Started successfully')
    end

    it 'shows nothing if state is pending' do
      dc = build :docker_command, state: :pending
      expect(described_class.new(dc).state_details).to be nil
    end
  end
end
