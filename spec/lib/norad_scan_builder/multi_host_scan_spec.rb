# frozen_string_literal: true

require 'rails_helper'
require './lib/norad_scan_builder'
require 'lib/norad_scan_builder/shared_examples/norad_scan'

RSpec.describe NoradScanBuilder::MultiHostScan, with_resque_doubled: true do
  it_should_behave_like 'a Norad Scan' do
    let(:args_klass) { NoradScanBuilder::MultiHostScanArgs }
    let(:target_klass) { NoradScanBuilder::MultiHostScanTarget }
    let(:options) { { organization: double, machines: [double] } }
  end
end
