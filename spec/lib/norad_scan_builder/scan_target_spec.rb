# frozen_string_literal: true

require 'rails_helper'
require './lib/norad_scan_builder'
require 'lib/norad_scan_builder/shared_examples/scan_target'

RSpec.describe NoradScanBuilder::ScanTarget, with_resque_doubled: true do
  it_behaves_like 'a Scan Target'
end
