# frozen_string_literal: true

require 'rails_helper'
require './lib/norad_scan_builder'

RSpec.describe NoradScanBuilder::NoradScan, with_resque_doubled: true do
  describe '.new' do
    it 'raises a NotImplemented exception' do
      expect do
        described_class.new(double, double, double)
      end.to raise_error(NotImplementedError)
    end
  end
end
