# frozen_string_literal: true

require 'rails_helper'
require './lib/norad_scan_builder'
require 'lib/norad_scan_builder/shared_examples/scan_job_utilities'

RSpec.describe NoradScanBuilder::OrganizationScanJob, with_resque_doubled: true do
  let(:organization) { build_stubbed(:organization) }
  let(:container) { build_stubbed(:security_container) }
  let(:excluded_machines) { double }
  let(:secret) { build_stubbed(:security_container_secret) }
  let(:command) { build_stubbed(:docker_command) }

  describe 'instantiation' do
    it 'creates a container secret' do
      allow(NoradScanBuilder::MultiHostScan).to receive(:new)
      expect(SecurityContainerSecret).to receive(:create!).and_return(secret)
      described_class.new(organization, container, excluded_machines, command)
    end

    it 'creates a MultiHostScan' do
      expect(NoradScanBuilder::MultiHostScan).to receive(:new)
      allow(SecurityContainerSecret).to receive(:create!).and_return(secret)
      described_class.new(organization, container, excluded_machines, command)
    end
  end

  describe 'instance methods' do
    include_examples 'Scan Job Utilities' do
      let(:organization) { create(:organization) }
      let(:excluded_machines) { [create(:machine, organization: organization)] }
      let(:scan_job) { described_class.new(organization, container, excluded_machines, command) }
    end
  end
end
