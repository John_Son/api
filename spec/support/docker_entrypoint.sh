#!/bin/bash
set -e

nc -l 9248
cat $CI_PROJECT_DIR/hosts > /etc/hosts

# Reason this hack is needed: https://gitlab.com/gitlab-org/gitlab-runner/issues/1042#note_61788095
# Long story short, this is the only way "sibling" docker services can communicate with one
# another in gitlab ci, for now.
export POSTGRES_HOST=`cat /etc/hosts |grep postgres|cut -d$'\t' -f1`

bundle exec rails db:setup
bundle exec rails tmp:cache:clear

exec "$@"
