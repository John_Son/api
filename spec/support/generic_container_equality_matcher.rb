# frozen_string_literal: true

# Help RSpec out by telling it how two GenericContainers are equal instead of letting it
# rely on its default Object equality testers which causes tests to fail.
require Rails.root.join 'app', 'lib', 'generic_container.rb'
class GenericContainer
  def ==(other)
    other.full_path == full_path
  end
end
