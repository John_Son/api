# frozen_string_literal: true

require 'rails_helper'
require 'query_objects/shared_examples/query_object'
require 'query_objects/shared_examples/scan_history_limiting'

RSpec.describe OrganizationScanSummaryQuery do
  NUM_RESULTS = 4

  let(:org) { create :organization }
  let(:machine) { create :machine, organization: org }
  let(:instance) { described_class.new(relation: org) }
  let(:dc) { create :docker_command, commandable: org }

  it_behaves_like 'a Query Object'

  def create_results(statuses)
    NUM_RESULTS.times do
      assessment = create :white_box_assessment, docker_command: dc, machine: machine
      statuses.each { |status| create :result, assessment: assessment, status: status }
    end
  end

  describe '#assessments_summary' do
    it_behaves_like 'scan history limiting' do
      let(:commandable) { org }
    end

    context 'results_summary' do
      context 'when all statuses are the same' do
        before :each do
          create_results %i[pass fail error warn info]
        end

        it 'counts by status' do
          expect(instance.assessments_summary[dc.id][:results_summary]).to eq(
            'failing' => NUM_RESULTS,
            'passing' => NUM_RESULTS,
            'erroring' => NUM_RESULTS,
            'warning' => NUM_RESULTS,
            'informing' => NUM_RESULTS
          )
        end
      end

      context 'when statuses are different' do
        before :each do
          create_results %i[pass pass fail fail warn]
        end

        it 'counts by status' do
          expect(instance.assessments_summary[dc.id][:results_summary]).to eq(
            'failing' => NUM_RESULTS * 2,
            'passing' => NUM_RESULTS * 2,
            'warning' => NUM_RESULTS,
            'erroring' => 0,
            'informing' => 0
          )
        end
      end

      context 'when no results exist' do
        let(:dc_with_no_results) { create :docker_command, commandable: org }

        before :each do
          create :white_box_assessment, docker_command: dc_with_no_results

          # Create results for other docker commands
          create_results %i[pass]
        end

        it 'has all counts initialized to 0' do
          expect(instance.assessments_summary[dc_with_no_results.id][:results_summary]).to eq(
            'failing' => 0,
            'passing' => 0,
            'warning' => 0,
            'erroring' => 0,
            'informing' => 0
          )
        end
      end

      context 'when no assessments exist' do
        let!(:dc_with_no_assessments) { create :docker_command, commandable: org }

        before :each do
          # Create results for other docker commands
          create_results %i[pass]
        end

        it 'includes the empty docker command in the assessments summary' do
          expect(instance.assessments_summary).to have_key(dc_with_no_assessments.id)
          expect(instance.assessments_summary).to have_key(dc.id)
          expect(instance.assessments_summary.keys.size).to eq 2
        end
      end
    end

    it 'reduces by state and creation time' do
      dc1 = create :docker_command, commandable: org
      a1 = create :white_box_assessment, docker_command: dc1, state: 'complete', machine: machine
      create :white_box_assessment, docker_command: dc1, created_at: 1.day.ago, state: 'in_progress', machine: machine
      create :white_box_assessment, docker_command: dc1, created_at: 2.days.ago, machine: machine

      dc2 = create :docker_command, commandable: org
      a2 = create :white_box_assessment, docker_command: dc2, created_at: 1.day.ago, state: 'in_progress',
                                         machine: machine
      create :white_box_assessment, docker_command: dc2, created_at: 5.days.ago, state: 'complete', machine: machine
      create :white_box_assessment, docker_command: dc2, created_at: 2.days.ago, state: 'in_progress', machine: machine

      expect(instance.assessments_summary[dc1.id][:created_at].to_i).to eq(a1.created_at.to_i)
      expect(instance.assessments_summary[dc1.id][:state]).to eq 'pending_scheduling'
      expect(instance.assessments_summary[dc2.id][:created_at].to_i).to eq(a2.created_at.to_i)
      expect(instance.assessments_summary[dc2.id][:state]).to eq 'in_progress'
    end

    it 'reduces by machines_scanned' do
      dc1 = create :docker_command, commandable: org
      5.times do
        machine = create :machine, organization: org
        create :white_box_assessment, docker_command: dc1, machine: machine
      end

      expect(instance.assessments_summary[dc1.id][:machines_scanned]).to eq 5
    end
  end
end
