# frozen_string_literal: true

require 'rails_helper'

shared_examples_for 'scan history limiting' do
  let(:expected_ids) do
    DockerCommand
      .where(id: @dc_ids)
      .order(created_at: :desc)
      .limit(described_class::SCAN_HISTORY_LIMIT)
      .pluck(:id)
  end

  before do
    @dc_ids = []
    (described_class::SCAN_HISTORY_LIMIT + 1).times do
      dc = create :docker_command, commandable: commandable
      @dc_ids << dc.id
      wba = create :white_box_assessment, docker_command: dc, machine: machine
      create :result, assessment: wba
    end

    # Create other records to ensure they don't interfere with the data we're after
    other_org = create :organization
    other_machine = create :machine, organization: other_org
    5.times do
      create :docker_command, commandable: other_org
      create :docker_command, commandable: other_machine
    end
  end

  it 'includes only the latest SCAN_HISTORY_LIMIT' do
    expect(instance.assessments_summary.size).to eq described_class::SCAN_HISTORY_LIMIT
    expect(expected_ids).to match_array(instance.assessments_summary.keys)
  end
end
