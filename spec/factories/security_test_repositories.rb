# frozen_string_literal: true

# == Schema Information
#
# Table name: security_test_repositories
#
#  id                 :integer          not null, primary key
#  host               :string
#  name               :string           not null
#  public             :boolean          default(FALSE)
#  username           :string
#  password_encrypted :string
#  official           :boolean          default(FALSE)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
# Indexes
#
#  index_security_test_repositories_on_host  (host) UNIQUE
#  index_security_test_repositories_on_name  (name) UNIQUE
#

FactoryBot.define do
  factory :security_test_repository do
    sequence(:host) { |n| "factory-registry-#{n}:5000" }
    sequence(:name) { |n| "Test Repository #{n}" }

    factory :public_repository do
      public true

      factory :official_repository do
        host NORAD_REGISTRY
        official true
        allow_official_changes true
        initialize_with { SecurityTestRepository.find_or_create_by(host: NORAD_REGISTRY) }
      end
    end

    factory :private_repository do
      public false
      username 'norad'
      password 'goodpass'
    end
  end
end
