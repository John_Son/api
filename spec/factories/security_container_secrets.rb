# frozen_string_literal: true

# == Schema Information
#
# Table name: security_container_secrets
#
#  id               :integer          not null, primary key
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  secret_encrypted :string
#

FactoryBot.define do
  factory :security_container_secret do
    secret SecureRandom.hex
    factory :expired_security_container_secret do
      updated_at { 25.hours.ago }
    end
  end
end
