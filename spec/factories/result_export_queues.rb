# frozen_string_literal: true

# == Schema Information
#
# Table name: result_export_queues
#
#  id              :integer          not null, primary key
#  organization_id :integer          not null
#  created_by      :string           not null
#  type            :string           not null
#  auto_sync       :boolean          default(FALSE), not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_result_export_queues_on_organization_id           (organization_id)
#  index_result_export_queues_on_organization_id_and_type  (organization_id,type) UNIQUE WHERE ((type)::text = 'InfosecExportQueue'::text)
#  index_result_export_queues_on_type                      (type)
#
# Foreign Keys
#
#  fk_rails_e5fc28c9ec  (organization_id => organizations.id) ON DELETE => cascade
#

FactoryBot.define do
  factory :result_export_queue do
    association :organization
    auto_sync false
    type 'InfosecExportQueue'
    created_at DateTime.now.iso8601
    updated_at DateTime.now.iso8601
    created_by { create(:user).uid }

    factory :jira_export_queue, class: 'JiraExportQueue' do
      type 'JiraExportQueue'
      after(:build) do |jp|
        jp.custom_jira_configuration = build(:custom_jira_configuration, jira_export_queue: jp)
      end
    end

    factory :infosec_export_queue, class: 'InfosecExportQueue' do
      type 'InfosecExportQueue'
      after(:build) do |i|
        i.infosec_export_queue_configuration = build(:infosec_export_queue_configuration, infosec_export_queue: i)
      end
    end
  end
end
