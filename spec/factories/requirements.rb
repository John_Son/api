# frozen_string_literal: true

# == Schema Information
#
# Table name: requirements
#
#  id                   :integer          not null, primary key
#  name                 :string
#  description          :string
#  requirement_group_id :integer          not null
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#
# Indexes
#
#  index_requirements_on_requirement_group_id  (requirement_group_id)
#
# Foreign Keys
#
#  fk_rails_cce12a8273  (requirement_group_id => requirement_groups.id) ON DELETE => cascade
#

FactoryBot.define do
  factory :requirement do
    sequence(:name) { |n| "req-#{n}" }
    description 'MyString'
    requirement_group
  end
end
