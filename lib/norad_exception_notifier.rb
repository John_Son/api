# frozen_string_literal: true

module NoradExceptionNotifier
  extend ActiveSupport::Concern

  def notify_airbrake(exception)
    return unless Rails.env.production?
    Airbrake.notify_sync exception
  end

  class_methods do
    def notify_airbrake(exception)
      return unless Rails.env.production?
      Airbrake.notify_sync exception
    end
  end
end
