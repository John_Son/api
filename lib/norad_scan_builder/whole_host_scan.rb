# frozen_string_literal: true

module NoradScanBuilder
  # Scan a single host with one set of arguments
  class WholeHostScan < NoradScan
    def post_initialize(opts)
      machine = opts.fetch(:machine)
      @args = HostScanArgs.new(container, machine: machine)
      @target = ScanTarget.new(container, secret_id, dc, machine) unless @args.to_a.empty?
    end
  end
end
