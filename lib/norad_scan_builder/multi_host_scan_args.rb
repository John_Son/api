# frozen_string_literal: true

module NoradScanBuilder
  class MultiHostScanArgs < ScanArgs
    attr_reader :organization, :machines

    def initialize(container, opts = {})
      @organization = opts.fetch(:organization)
      @machines = opts.fetch(:machines)
      super
    end

    private

    def args_array
      cargs = container_config || {}
      args = { target: machines.map(&:target_address).join(',') }.merge(cargs)
      create_command_array(args)
    end

    def container_config
      config = container.config_for_organization(organization.id)
      config ? config.args_hash : container.args_hash
    end
  end
end
