# frozen_string_literal: true

module NoradScanBuilder
  class ScanArgs
    attr_reader :container

    def initialize(container, _opts = {})
      @container = container
    end

    def to_a
      args_array
    rescue StandardError => e
      Resque.logger.error "There was an error building the arguments for #{container.name}."
      Resque.logger.error "The following error occurred: #{e.message}."
      Resque.logger.error e.backtrace
      []
    end

    private

    def create_command_array(args_hash)
      formatted_prog_args_string(container.prog_args, args_hash).split(' ').map do |arg|
        arg.gsub(/#{SecurityContainer::SPACE_PLACEHOLDER}/, ' ')
      end
    end

    def formatted_prog_args_string(prog_args, args_hash)
      formatted = format(prog_args, args_hash).gsub(/\s*-\S+\s*#{SecurityContainerConfig::EMPTY_CONFIG_VALUE}/, '')
      formatted.gsub(/#{SecurityContainerConfig::EMPTY_CONFIG_VALUE}/, '')
    end

    def container_config(machine)
      config = container.config_for_machine(machine.id) || container.config_for_organization(machine.organization.id)
      config ? config.args_hash : container.args_hash
    end
  end
end
