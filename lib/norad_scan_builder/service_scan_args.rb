# frozen_string_literal: true

module NoradScanBuilder
  class ServiceScanArgs < ScanArgs
    attr_reader :service

    def initialize(container, opts = {})
      @service = opts.fetch(:service)
      super
    end

    private

    def args_array
      sargs = service.service_configuration.merge(target: service.machine.target_address)
      cargs = container_config(service.machine) || {}
      create_command_array(sargs.merge(cargs))
    end
  end
end
