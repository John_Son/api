# frozen_string_literal: true

pathname = Rails.root.join('db', 'seeds', "#{Rails.env}.rb")
load pathname if File.exist? pathname
