# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180730155733) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "uuid-ossp"

  create_table "api_tokens", force: :cascade do |t|
    t.string   "value",                  null: false
    t.integer  "state",      default: 0, null: false
    t.integer  "user_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["user_id"], name: "index_api_tokens_on_user_id", using: :btree
    t.index ["value"], name: "index_api_tokens_on_value", unique: true, using: :btree
  end

  create_table "application_types", force: :cascade do |t|
    t.integer  "port",                           null: false
    t.string   "name",                           null: false
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "transport_protocol", default: 0, null: false
    t.index ["port"], name: "index_application_types_on_port", using: :btree
  end

  create_table "assessments", force: :cascade do |t|
    t.string   "identifier",                               null: false
    t.integer  "machine_id"
    t.integer  "state",                        default: 0, null: false
    t.string   "title"
    t.string   "description"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.integer  "security_container_id"
    t.integer  "security_container_secret_id"
    t.integer  "docker_command_id"
    t.datetime "state_transition_time",                    null: false
    t.integer  "service_id"
    t.string   "type",                                     null: false
    t.index ["docker_command_id"], name: "index_assessments_on_docker_command_id", using: :btree
    t.index ["identifier"], name: "index_assessments_on_identifier", unique: true, using: :btree
    t.index ["machine_id"], name: "index_assessments_on_machine_id", using: :btree
    t.index ["security_container_id"], name: "index_assessments_on_security_container_id", using: :btree
    t.index ["security_container_secret_id"], name: "index_assessments_on_security_container_secret_id", using: :btree
    t.index ["service_id"], name: "index_assessments_on_service_id", using: :btree
  end

  create_table "authentication_methods", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["type"], name: "index_authentication_methods_on_type", using: :btree
    t.index ["user_id"], name: "index_authentication_methods_on_user_id", unique: true, using: :btree
  end

  create_table "custom_jira_configurations", force: :cascade do |t|
    t.string   "title"
    t.string   "site_url",               null: false
    t.string   "project_key",            null: false
    t.string   "username_encrypted"
    t.string   "password_encrypted"
    t.integer  "result_export_queue_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["result_export_queue_id"], name: "index_custom_jira_configurations_on_result_export_queue_id", using: :btree
  end

  create_table "docker_commands", force: :cascade do |t|
    t.text     "error_details"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.integer  "machine_id"
    t.integer  "organization_id"
    t.integer  "state",                 default: 0, null: false
    t.integer  "assessments_in_flight", default: 0
    t.datetime "started_at"
    t.datetime "finished_at"
    t.index ["machine_id"], name: "index_docker_commands_on_machine_id", using: :btree
    t.index ["organization_id"], name: "index_docker_commands_on_organization_id", using: :btree
  end

  create_table "docker_relays", force: :cascade do |t|
    t.integer  "organization_id",                               null: false
    t.text     "public_key",                                    null: false
    t.string   "queue_name",                                    null: false
    t.integer  "state",                         default: 1,     null: false
    t.datetime "last_heartbeat",                                null: false
    t.boolean  "verified",                      default: false, null: false
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.string   "key_signature",                                 null: false
    t.string   "file_encryption_key_encrypted"
    t.string   "last_reported_version"
    t.boolean  "outdated",                      default: false, null: false
    t.index ["organization_id"], name: "index_docker_relays_on_organization_id", using: :btree
    t.index ["outdated"], name: "index_docker_relays_on_outdated", using: :btree
    t.index ["queue_name"], name: "index_docker_relays_on_queue_name", unique: true, using: :btree
  end

  create_table "enforcements", force: :cascade do |t|
    t.integer  "requirement_group_id", null: false
    t.integer  "organization_id",      null: false
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.index ["organization_id"], name: "index_enforcements_on_organization_id", using: :btree
    t.index ["requirement_group_id", "organization_id"], name: "index_enforcements_on_requirement_group_id_and_organization_id", unique: true, using: :btree
    t.index ["requirement_group_id"], name: "index_enforcements_on_requirement_group_id", using: :btree
  end

  create_table "iaas_configurations", force: :cascade do |t|
    t.integer  "provider",        null: false
    t.string   "user_encrypted"
    t.string   "key_encrypted"
    t.string   "region"
    t.string   "project"
    t.string   "auth_url"
    t.integer  "organization_id", null: false
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["organization_id"], name: "index_iaas_configurations_on_organization_id", unique: true, using: :btree
  end

  create_table "iaas_discoveries", force: :cascade do |t|
    t.integer  "iaas_configuration_id",             null: false
    t.integer  "state",                 default: 0, null: false
    t.string   "error_message"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.index ["iaas_configuration_id", "state"], name: "index_iaas_discoveries_on_ip_state", unique: true, where: "(state = 1)", using: :btree
    t.index ["iaas_configuration_id", "state"], name: "index_iaas_discoveries_on_pending_state", unique: true, where: "(state = 0)", using: :btree
  end

  create_table "infosec_export_queue_configurations", force: :cascade do |t|
    t.string   "ctsm_id"
    t.integer  "result_export_queue_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["result_export_queue_id"], name: "index_ieq_configs_on_result_export_queue_id", using: :btree
  end

  create_table "local_authentication_records", force: :cascade do |t|
    t.string   "password_digest"
    t.string   "password_reset_token"
    t.datetime "password_reset_sent_at"
    t.integer  "authentication_method_id"
    t.string   "email_confirmation_token"
    t.datetime "email_confirmed_at"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.index ["authentication_method_id"], name: "index_local_authentication_records_on_authentication_method_id", unique: true, using: :btree
    t.index ["email_confirmation_token"], name: "index_local_authentication_records_on_email_confirmation_token", unique: true, using: :btree
    t.index ["email_confirmed_at"], name: "index_local_authentication_records_on_email_confirmed_at", using: :btree
    t.index ["password_reset_sent_at"], name: "index_local_authentication_records_on_password_reset_sent_at", using: :btree
    t.index ["password_reset_token"], name: "index_local_authentication_records_on_password_reset_token", unique: true, using: :btree
  end

  create_table "machine_connectivity_checks", force: :cascade do |t|
    t.integer  "machine_id",                   null: false
    t.integer  "security_container_secret_id"
    t.datetime "finished_at"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "status"
    t.string   "type",                         null: false
    t.text     "status_reason"
    t.index ["finished_at"], name: "index_machine_connectivity_checks_on_finished_at", using: :btree
    t.index ["machine_id", "type"], name: "index_machine_connectivity_checks_on_machine_id_and_type", using: :btree
    t.index ["security_container_secret_id"], name: "index_machine_connectivity_checks_on_s_c_s_id", unique: true, using: :btree
    t.index ["status"], name: "index_machine_connectivity_checks_on_status", using: :btree
    t.index ["updated_at"], name: "index_machine_connectivity_checks_on_updated_at", using: :btree
  end

  create_table "machine_scan_schedules", force: :cascade do |t|
    t.integer  "machine_id",             null: false
    t.integer  "period",     default: 0, null: false
    t.string   "at",                     null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["machine_id"], name: "index_machine_scan_schedules_on_machine_id", unique: true, using: :btree
  end

  create_table "machines", force: :cascade do |t|
    t.integer  "organization_id"
    t.inet     "ip"
    t.string   "fqdn"
    t.text     "description"
    t.integer  "machine_status",     default: 0,     null: false
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.string   "name",                               null: false
    t.boolean  "use_fqdn_as_target", default: false, null: false
    t.index ["name", "organization_id"], name: "index_machines_on_name_and_organization_id", unique: true, using: :btree
    t.index ["organization_id"], name: "index_machines_on_organization_id", using: :btree
  end

  create_table "memberships", force: :cascade do |t|
    t.integer  "user_id",         null: false
    t.integer  "organization_id", null: false
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["organization_id"], name: "index_memberships_on_organization_id", using: :btree
    t.index ["user_id", "organization_id"], name: "index_memberships_on_user_id_and_organization_id", unique: true, using: :btree
    t.index ["user_id"], name: "index_memberships_on_user_id", using: :btree
  end

  create_table "notification_channels", force: :cascade do |t|
    t.boolean  "enabled",         default: true
    t.string   "event"
    t.integer  "organization_id"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.index ["event"], name: "index_notification_channels_on_event", using: :btree
    t.index ["organization_id", "event"], name: "index_notification_channels_on_organization_id_and_event", unique: true, using: :btree
  end

  create_table "organization_configurations", force: :cascade do |t|
    t.integer  "organization_id",                            null: false
    t.boolean  "auto_approve_docker_relays", default: false, null: false
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.boolean  "use_relay_ssh_key",          default: false, null: false
    t.index ["organization_id"], name: "index_organization_configurations_on_organization_id", unique: true, using: :btree
  end

  create_table "organization_errors", force: :cascade do |t|
    t.integer  "organization_id"
    t.string   "type"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "errable_type"
    t.integer  "errable_id"
    t.index ["organization_id", "errable_id", "errable_type", "type"], name: "index_organization_errors_on_o_id_and_type_and_e_type_and_e_id", unique: true, using: :btree
  end

  create_table "organization_scan_schedules", force: :cascade do |t|
    t.integer  "organization_id",             null: false
    t.integer  "period",          default: 0, null: false
    t.string   "at",                          null: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.index ["organization_id"], name: "index_organization_scan_schedules_on_organization_id", unique: true, using: :btree
  end

  create_table "organization_tokens", force: :cascade do |t|
    t.string   "value",                       null: false
    t.integer  "state",           default: 0, null: false
    t.integer  "organization_id",             null: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.index ["organization_id"], name: "index_organization_tokens_on_organization_id", using: :btree
    t.index ["value"], name: "index_organization_tokens_on_value", unique: true, using: :btree
  end

  create_table "organizations", force: :cascade do |t|
    t.string   "uid",           null: false
    t.string   "slug",          null: false
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.uuid     "exchange_name", null: false
    t.index ["exchange_name"], name: "index_organizations_on_exchange_name", unique: true, using: :btree
    t.index ["slug"], name: "index_organizations_on_slug", unique: true, using: :btree
    t.index ["uid"], name: "index_organizations_on_uid", unique: true, using: :btree
  end

  create_table "provisions", force: :cascade do |t|
    t.integer  "security_container_id", null: false
    t.integer  "requirement_id",        null: false
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.index ["requirement_id"], name: "index_provisions_on_requirement_id", using: :btree
    t.index ["security_container_id"], name: "index_provisions_on_security_container_id", using: :btree
  end

  create_table "repository_members", force: :cascade do |t|
    t.integer  "user_id",                     null: false
    t.integer  "security_test_repository_id", null: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.index ["security_test_repository_id"], name: "index_repository_members_on_security_test_repository_id", using: :btree
    t.index ["user_id", "security_test_repository_id"], name: "index_repository_members_on_u_id_and_s_t_r_id", unique: true, using: :btree
    t.index ["user_id"], name: "index_repository_members_on_user_id", using: :btree
  end

  create_table "repository_whitelist_entries", force: :cascade do |t|
    t.integer  "organization_id",             null: false
    t.integer  "security_test_repository_id", null: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.index ["organization_id", "security_test_repository_id"], name: "index_r_w_entries_on_o_id_and_s_t_r_id", unique: true, using: :btree
  end

  create_table "requirement_groups", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "requirements", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "requirement_group_id", null: false
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.index ["requirement_group_id"], name: "index_requirements_on_requirement_group_id", using: :btree
  end

  create_table "result_export_queues", force: :cascade do |t|
    t.integer  "organization_id",                 null: false
    t.string   "created_by",                      null: false
    t.string   "type",                            null: false
    t.boolean  "auto_sync",       default: false, null: false
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.index ["organization_id", "type"], name: "index_result_export_queues_on_organization_id_and_type", unique: true, where: "((type)::text = 'InfosecExportQueue'::text)", using: :btree
    t.index ["organization_id"], name: "index_result_export_queues_on_organization_id", using: :btree
    t.index ["type"], name: "index_result_export_queues_on_type", using: :btree
  end

  create_table "result_ignore_rules", force: :cascade do |t|
    t.integer  "ignore_scope_id",   null: false
    t.string   "ignore_scope_type", null: false
    t.string   "signature",         null: false
    t.text     "comment"
    t.string   "created_by",        null: false
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["ignore_scope_type", "ignore_scope_id", "signature"], name: "index_result_ignore_rules_on_i_s_type_and_i_s_id_and_signature", unique: true, using: :btree
    t.index ["ignore_scope_type", "ignore_scope_id"], name: "index_result_ignore_rules_on_i_s_type_and_i_s_id", using: :btree
    t.index ["signature"], name: "index_result_ignore_rules_on_signature", using: :btree
  end

  create_table "results", force: :cascade do |t|
    t.integer  "status",                default: 0,     null: false
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.integer  "sir",                   default: 0,     null: false
    t.integer  "assessment_id"
    t.boolean  "ignored",               default: false, null: false
    t.string   "signature",                             null: false
    t.boolean  "reported",              default: false, null: false
    t.string   "title_encrypted"
    t.string   "description_encrypted"
    t.string   "output_encrypted"
    t.string   "nid_encrypted"
    t.index ["assessment_id"], name: "index_results_on_assessment_id", using: :btree
    t.index ["signature"], name: "index_results_on_signature", using: :btree
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.string   "resource_type"
    t.integer  "resource_id",   null: false
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
    t.index ["name"], name: "index_roles_on_name", using: :btree
  end

  create_table "scan_containers", force: :cascade do |t|
    t.integer  "docker_command_id",     null: false
    t.integer  "security_container_id", null: false
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.index ["docker_command_id", "security_container_id"], name: "index_scan_containers_on_d_c_id_and_s_c_id", unique: true, using: :btree
  end

  create_table "security_container_configs", force: :cascade do |t|
    t.integer "security_container_id"
    t.boolean "enabled_outside_of_requirement", default: false, null: false
    t.integer "machine_id"
    t.integer "organization_id"
    t.string  "values_encrypted"
    t.index ["machine_id"], name: "index_security_container_configs_on_machine_id", using: :btree
    t.index ["organization_id"], name: "index_security_container_configs_on_organization_id", using: :btree
    t.index ["security_container_id"], name: "index_security_container_configs_on_security_container_id", using: :btree
  end

  create_table "security_container_secrets", force: :cascade do |t|
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "secret_encrypted"
  end

  create_table "security_containers", force: :cascade do |t|
    t.string   "name",                                        null: false
    t.integer  "category",                    default: 0,     null: false
    t.string   "prog_args",                                   null: false
    t.jsonb    "default_config"
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.boolean  "multi_host",                  default: false, null: false
    t.string   "test_types",                  default: [],    null: false, array: true
    t.boolean  "configurable",                default: false, null: false
    t.integer  "application_type_id"
    t.string   "help_url"
    t.integer  "security_test_repository_id",                 null: false
    t.index ["application_type_id"], name: "index_security_containers_on_application_type_id", using: :btree
    t.index ["security_test_repository_id", "name"], name: "index_security_containers_on_s_t_r_id_and_name", unique: true, using: :btree
    t.index ["security_test_repository_id"], name: "index_security_containers_on_security_test_repository_id", using: :btree
  end

  create_table "security_test_repositories", force: :cascade do |t|
    t.string   "host"
    t.string   "name",                               null: false
    t.boolean  "public",             default: false
    t.string   "username"
    t.string   "password_encrypted"
    t.boolean  "official",           default: false
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.index ["host"], name: "index_security_test_repositories_on_host", unique: true, using: :btree
    t.index ["name"], name: "index_security_test_repositories_on_name", unique: true, using: :btree
  end

  create_table "service_discoveries", force: :cascade do |t|
    t.integer  "machine_id"
    t.integer  "container_secret_id"
    t.integer  "state"
    t.string   "error_message"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.index ["container_secret_id"], name: "index_service_discoveries_on_container_secret_id", using: :btree
    t.index ["machine_id", "state"], name: "index_service_discoveries_on_in_progress_state", unique: true, where: "(state = 1)", using: :btree
    t.index ["machine_id", "state"], name: "index_service_discoveries_on_pending_state", unique: true, where: "(state = 0)", using: :btree
    t.index ["machine_id"], name: "index_service_discoveries_on_machine_id", using: :btree
  end

  create_table "service_identities", force: :cascade do |t|
    t.string   "username_encrypted"
    t.string   "password_encrypted"
    t.integer  "service_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["service_id"], name: "index_service_identities_on_service_id", using: :btree
  end

  create_table "services", force: :cascade do |t|
    t.string   "name",                                null: false
    t.text     "description"
    t.integer  "port",                                null: false
    t.integer  "port_type",           default: 0,     null: false
    t.integer  "encryption_type",     default: 0,     null: false
    t.integer  "machine_id"
    t.string   "type",                                null: false
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.boolean  "allow_brute_force",   default: false, null: false
    t.integer  "application_type_id"
    t.boolean  "discovered",          default: false, null: false
    t.index ["application_type_id"], name: "index_services_on_application_type_id", using: :btree
    t.index ["machine_id", "port"], name: "index_services_on_machine_id_and_port", unique: true, using: :btree
    t.index ["machine_id"], name: "index_services_on_machine_id", using: :btree
    t.index ["type"], name: "index_services_on_type", using: :btree
  end

  create_table "ssh_key_pair_assignments", force: :cascade do |t|
    t.integer  "machine_id",      null: false
    t.integer  "ssh_key_pair_id", null: false
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["machine_id"], name: "index_ssh_key_pair_assignments_on_machine_id", unique: true, using: :btree
    t.index ["ssh_key_pair_id"], name: "index_ssh_key_pair_assignments_on_ssh_key_pair_id", using: :btree
  end

  create_table "ssh_key_pairs", force: :cascade do |t|
    t.string   "name",               null: false
    t.text     "description"
    t.string   "username_encrypted"
    t.string   "key_encrypted"
    t.string   "key_signature",      null: false
    t.integer  "organization_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["name", "organization_id"], name: "index_ssh_key_pairs_on_name_and_organization_id", unique: true, using: :btree
    t.index ["organization_id"], name: "index_ssh_key_pairs_on_organization_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",      null: false
    t.string   "uid",        null: false
    t.string   "firstname"
    t.string   "lastname"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["uid"], name: "index_users_on_uid", unique: true, using: :btree
  end

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree
  end

  create_table "web_application_configs", force: :cascade do |t|
    t.integer  "auth_type",                      default: 0,   null: false
    t.string   "url_blacklist"
    t.string   "starting_page_path",             default: "/", null: false
    t.string   "login_form_username_field_name"
    t.string   "login_form_password_field_name"
    t.integer  "service_id"
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.index ["service_id"], name: "index_web_application_configs_on_service_id", using: :btree
  end

  add_foreign_key "api_tokens", "users", on_delete: :cascade
  add_foreign_key "assessments", "docker_commands", on_delete: :cascade
  add_foreign_key "assessments", "machines", on_delete: :cascade
  add_foreign_key "assessments", "security_container_secrets", on_delete: :nullify
  add_foreign_key "assessments", "security_containers", on_delete: :cascade
  add_foreign_key "assessments", "services", on_delete: :cascade
  add_foreign_key "custom_jira_configurations", "result_export_queues", on_delete: :cascade
  add_foreign_key "docker_commands", "machines", on_delete: :cascade
  add_foreign_key "docker_commands", "organizations", on_delete: :cascade
  add_foreign_key "docker_relays", "organizations", on_delete: :cascade
  add_foreign_key "enforcements", "organizations", on_delete: :cascade
  add_foreign_key "enforcements", "requirement_groups", on_delete: :cascade
  add_foreign_key "iaas_configurations", "organizations", on_delete: :cascade
  add_foreign_key "iaas_discoveries", "iaas_configurations", on_delete: :cascade
  add_foreign_key "infosec_export_queue_configurations", "result_export_queues", on_delete: :cascade
  add_foreign_key "machine_connectivity_checks", "machines", on_delete: :cascade
  add_foreign_key "machine_connectivity_checks", "security_container_secrets", on_delete: :nullify
  add_foreign_key "machine_scan_schedules", "machines", on_delete: :cascade
  add_foreign_key "machines", "organizations", on_delete: :cascade
  add_foreign_key "memberships", "organizations", on_delete: :cascade
  add_foreign_key "memberships", "users", on_delete: :cascade
  add_foreign_key "organization_configurations", "organizations", on_delete: :cascade
  add_foreign_key "organization_errors", "organizations", on_delete: :cascade
  add_foreign_key "organization_scan_schedules", "organizations", on_delete: :cascade
  add_foreign_key "organization_tokens", "organizations", on_delete: :cascade
  add_foreign_key "provisions", "requirements", on_delete: :cascade
  add_foreign_key "provisions", "security_containers", on_delete: :cascade
  add_foreign_key "repository_members", "security_test_repositories", on_delete: :cascade
  add_foreign_key "repository_members", "users", on_delete: :cascade
  add_foreign_key "repository_whitelist_entries", "organizations", on_delete: :cascade
  add_foreign_key "repository_whitelist_entries", "security_test_repositories", on_delete: :cascade
  add_foreign_key "requirements", "requirement_groups", on_delete: :cascade
  add_foreign_key "result_export_queues", "organizations", on_delete: :cascade
  add_foreign_key "results", "assessments", on_delete: :cascade
  add_foreign_key "scan_containers", "docker_commands", on_delete: :cascade
  add_foreign_key "scan_containers", "security_containers", on_delete: :cascade
  add_foreign_key "security_container_configs", "machines", on_delete: :cascade
  add_foreign_key "security_container_configs", "organizations", on_delete: :cascade
  add_foreign_key "security_container_configs", "security_containers", on_delete: :cascade
  add_foreign_key "security_containers", "application_types"
  add_foreign_key "security_containers", "security_test_repositories", on_delete: :cascade
  add_foreign_key "service_discoveries", "machines", on_delete: :cascade
  add_foreign_key "service_discoveries", "security_container_secrets", column: "container_secret_id", on_delete: :nullify
  add_foreign_key "service_identities", "services", on_delete: :cascade
  add_foreign_key "services", "application_types"
  add_foreign_key "services", "machines", on_delete: :cascade
  add_foreign_key "ssh_key_pair_assignments", "machines", on_delete: :cascade
  add_foreign_key "ssh_key_pair_assignments", "ssh_key_pairs", on_delete: :cascade
  add_foreign_key "ssh_key_pairs", "organizations", on_delete: :cascade
  add_foreign_key "web_application_configs", "services", on_delete: :cascade
end
