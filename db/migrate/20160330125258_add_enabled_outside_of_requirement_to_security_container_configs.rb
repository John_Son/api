class AddEnabledOutsideOfRequirementToSecurityContainerConfigs < ActiveRecord::Migration[4.2]
  def change
    add_column :security_container_configs, :enabled_outside_of_requirement, :boolean, default: false, null: false
  end
end
