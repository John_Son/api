class CreateInfosecExportQueueConfigurationTable < ActiveRecord::Migration[5.0]
  def change
    create_table :infosec_export_queue_configurations do |t|
      t.string :ctsm_id
      t.belongs_to :result_export_queue, index: { name: 'index_ieq_configs_on_result_export_queue_id' }
      t.timestamps
    end

    add_foreign_key :infosec_export_queue_configurations, :result_export_queues, on_delete: :cascade
  end
end
