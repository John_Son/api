class AddEnforceRequirementsToOrganizationConfiguration < ActiveRecord::Migration[4.2]
  def change
    add_column :organization_configurations, :enforce_requirements, :boolean, null: false, default: false
  end
end
