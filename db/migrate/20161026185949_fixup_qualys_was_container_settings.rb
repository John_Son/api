class FixupQualysWasContainerSettings < ActiveRecord::Migration[4.2]
  def up
    qualys_was = SecurityContainer.find_by(name: 'norad-registry.cisco.com:5000/qualys-was:0.0.1')
    return unless qualys_was
    qualys_was.prog_args = '-h %{target} -r %{web_service_protocol}://%{target}:%{port}{web_service_starting_page_page_path} -u %{qualys_username} -p %{qualys_password} -w %{service_username} -v %{service_password} -t %{option_profile} -q %{qualys_scanner} -b %{web_service_url_blacklist}'
    qualys_was.default_config = { qualys_username: 'placeholder_qualys_username', qualys_password: 'placeholder_qualys_password', option_profile: 'NoradProfile', qualys_scanner: 'RTP-3' }
    qualys_was.configurable = true
    qualys_was.save!
    qualys_was.security_container_configs.each do |config|
      new_vals = config.values.dup
      new_vals.delete('target_url')
      new_vals.delete('web_username')
      new_vals.delete('web_password')
      new_vals.delete('coma_sep_blacklist_urls')
      config.values = new_vals
      config.save!
    end
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
