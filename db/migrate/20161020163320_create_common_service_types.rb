class CreateCommonServiceTypes < ActiveRecord::Migration[4.2]
  def change
    create_table :common_service_types do |t|
      t.integer :port, index: true, unique: true, null: false
      t.string :name, null: false

      t.timestamps null: false
    end
  end
end
