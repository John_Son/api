class CreateEnforcements < ActiveRecord::Migration[4.2]
  def change
    create_table :enforcements do |t|
      t.references :requirement_group, index: true, null: false
      t.references :organization, index: true, null: false

      t.timestamps null: false
    end
    add_foreign_key :enforcements, :requirement_groups, on_delete: :cascade
    add_foreign_key :enforcements, :organizations, on_delete: :cascade
    add_index :enforcements, [:requirement_group_id, :organization_id], unique: true
  end
end
