class AddStateTransitionTimeToWhiteBoxAssessments < ActiveRecord::Migration[4.2]
  class Assessment < ApplicationRecord
    self.abstract_class = true
  end

  class WhiteBoxAssessment < Assessment
    self.table_name = 'white_box_assessments'
  end

  def up
    add_column :white_box_assessments, :state_transition_time, :timestamp
    WhiteBoxAssessment.reset_column_information
    WhiteBoxAssessment.all.each do |assessment|
      assessment.update_column(:state_transition_time, assessment.updated_at)
    end
    change_column_null :white_box_assessments, :state_transition_time, false
  end

  def down
    remove_column :white_box_assessments, :state_transition_time
  end
end
