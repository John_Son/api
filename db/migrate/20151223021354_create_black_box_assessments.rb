class CreateBlackBoxAssessments < ActiveRecord::Migration[4.2]
  def change
    create_table :black_box_assessments do |t|
      t.string :identifier
      t.references :machine, index: true
      t.references :command, polymorphic: true, index: true
      t.integer :state, default: 0, null: false
      t.integer :category, default: 0, null: false
      t.string :title
      t.string :description
      t.references :security_container, index: true, foreign_key: true

      t.timestamps null: false
    end
    add_index :black_box_assessments, :identifier, unique: true
    add_foreign_key :black_box_assessments, :machines, on_delete: :cascade
  end
end
