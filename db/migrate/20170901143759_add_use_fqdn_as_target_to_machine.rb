class AddUseFqdnAsTargetToMachine < ActiveRecord::Migration[5.0]
  def change
    add_column :machines, :use_fqdn_as_target, :boolean, default: false, null: false
  end
end
