class CreateIaasDiscoveries < ActiveRecord::Migration[4.2]
  def change
    create_table :iaas_discoveries do |t|
      t.integer :iaas_configuration_id, null: false
      t.integer :state, null: false, default: 0
      t.string :error_message

      t.timestamps null: false
    end

    add_index :iaas_discoveries, [:iaas_configuration_id, :state], where: "state = #{IaasDiscovery::IN_PROGRESS_STATE}", unique: true, name: 'index_iaas_discoveries_on_ip_state'
    add_index :iaas_discoveries, [:iaas_configuration_id, :state], where: "state = #{IaasDiscovery::PENDING_STATE}", unique: true, name: 'index_iaas_discoveries_on_pending_state'
    add_foreign_key :iaas_discoveries, :iaas_configurations, on_delete: :cascade
  end
end
