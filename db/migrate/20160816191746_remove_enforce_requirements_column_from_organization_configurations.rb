class RemoveEnforceRequirementsColumnFromOrganizationConfigurations < ActiveRecord::Migration[4.2]
  def up
    remove_column :organization_configurations, :enforce_requirements
  end

  def down
    add :organization_configurations, :enforce_requirements, :boolean, null: false, default: false
  end
end
