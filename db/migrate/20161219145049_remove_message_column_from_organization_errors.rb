class RemoveMessageColumnFromOrganizationErrors < ActiveRecord::Migration[5.0]
  def up
    remove_column :organization_errors, :message
  end

  def down
    add_column :organization_errors, :message, :string
  end
end
