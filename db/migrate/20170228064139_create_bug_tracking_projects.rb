class CreateBugTrackingProjects < ActiveRecord::Migration[5.0]
  def change
    create_table :bug_tracking_projects do |t|
      t.belongs_to :organization
      t.string :title
      t.integer :bug_tracker
      t.string :site_url
      t.string :project_key
      t.string :username
      t.string :password_encrypted
      t.string :created_by
      t.datetime :created_at
      t.boolean :login_validated, null: false, default: false
      t.boolean :bugs_created, null: false, default: false
      t.boolean :active_project

      t.timestamps
    end

    add_foreign_key :bug_tracking_projects, :organizations, on_delete: :cascade
  end
end
