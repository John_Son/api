# Since these models may not exist later on, define them here so the migration will always run in the future.
class DockerCommand < ApplicationRecord
end

class ScanContainer < ApplicationRecord
end

class SecurityTestRepository < ApplicationRecord
  has_many :security_containers
end

class SecurityContainer < ApplicationRecord
  belongs_to :security_test_repository
end

class CreateScanContainers < ActiveRecord::Migration[5.0]
  def up
    create_table :scan_containers do |t|
      t.references :docker_command, foreign_key: { on_delete: :cascade }, index: false, null: false
      t.references :security_container, foreign_key: { on_delete: :cascade }, index: false, null: false
      t.index %i[docker_command_id security_container_id],
              unique: true,
              name: :index_scan_containers_on_d_c_id_and_s_c_id

      t.timestamps
    end
    create_scan_containers
    remove_column :docker_commands, :containers
  end

  def down
    add_column :docker_commands, :containers, :json
    create_dc_containers
    drop_table :scan_containers
  end

  private

  def by_full_paths(hosts, names)
    SecurityContainer
      .joins(:security_test_repository)
      .where(security_test_repositories: { host: hosts })
      .where(security_containers: { name: names })
  end

  def create_scan_containers
    DockerCommand.find_each do |dc|
      hosts = dc.containers.map { |c| c.split('/').first }
      names = dc.containers.map { |c| c.split('/').last }
      containers = by_full_paths(hosts, names)
      scan_container_params = containers.pluck(:id).map do |container_id|
        {
          security_container_id: container_id,
          docker_command_id: dc.id,
          created_at: dc.created_at,
          updated_at: dc.updated_at
        }
      end
      ScanContainer.create!(scan_container_params)
    end
  end

  def create_dc_containers
    DockerCommand.find_each do |dc|
      container_ids = ScanContainer.where(docker_command_id: dc.id).pluck(:security_container_id)
      container_full_paths = SecurityContainer.where(id: container_ids).map do |container|
        container.security_test_repository.host + '/' + container.name
      end
      dc.update_column(:containers, container_full_paths)
    end
  end
end
