class IndexMachineConnectivityChecksOnType < ActiveRecord::Migration[5.0]
  disable_ddl_transaction!

  def up
    add_index :machine_connectivity_checks, %i[machine_id type], algorithm: :concurrently
  end

  def down
    remove_index :machine_connectivity_checks, %i[machine_id type]
  end
end
