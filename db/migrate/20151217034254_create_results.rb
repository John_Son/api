class CreateResults < ActiveRecord::Migration[4.2]
  def change
    create_table :results do |t|
      t.references :assessment, polymorphic: true, index: true
      t.integer :status, default: 0, null: false
      t.text :output
      t.string :title
      t.string :description

      t.timestamps null: false
    end
  end
end
