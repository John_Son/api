class AddLastReportedVersionToDockerRelays < ActiveRecord::Migration[5.0]
  def change
    add_column :docker_relays, :last_reported_version, :string
    add_column :docker_relays, :outdated, :boolean, default: false, null: false
    add_index :docker_relays, :outdated
  end
end
