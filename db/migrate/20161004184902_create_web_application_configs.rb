class CreateWebApplicationConfigs < ActiveRecord::Migration[4.2]
  def change
    create_table :web_application_configs do |t|
      t.integer :auth_type, default: 0, null: false
      t.string :url_blacklist
      t.string :starting_page_path, default: '/', null: false
      t.string :login_form_username_field_name
      t.string :login_form_password_field_name
      t.references :service, index: true

      t.timestamps null: false
    end
    add_foreign_key :web_application_configs, :services, on_delete: :cascade
  end
end
