class RenameCommonServiceTypesToApplicationTypes < ActiveRecord::Migration[5.0]
  def change
    rename_index :common_service_types, :index_common_service_types_on_port, :index_application_types_on_port
    rename_table :common_service_types, :application_types

    rename_column :security_containers, :common_service_type_id, :application_type_id
    rename_column :services, :common_service_type_id, :application_type_id
  end
end
