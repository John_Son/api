class AddStatusToMachineConnectivityChecks < ActiveRecord::Migration[5.0]
  def change
    add_column :machine_connectivity_checks, :status, :integer
  end
end
