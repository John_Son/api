class UpdateSecurityContainerForeignKeyConstraintForWhiteBoxAssessments < ActiveRecord::Migration[4.2]
  def up
    remove_foreign_key :white_box_assessments, :security_containers
    add_foreign_key :white_box_assessments, :security_containers, on_delete: :cascade
  end

  def down
    add_foreign_key :white_box_assessments, :security_containers
  end
end
