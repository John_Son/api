class CreateUsers < ActiveRecord::Migration[4.2]
  def change
    create_table :users do |t|
      t.string :email, null: false
      t.string :uid, null: false
      t.string :firstname, null: false
      t.string :lastname, null: false

      t.timestamps null: false
    end
    add_index :users, :uid, unique: true
    add_index :users, :email, unique: true
  end
end
