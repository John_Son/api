class CreateSecurityContainers < ActiveRecord::Migration[4.2]
  def change
    create_table :security_containers do |t|
      t.string :name, null: false
      t.integer :category, default: 0, null: false
      t.string :prog_args, null: false
      t.json :default_config

      t.timestamps null: false
    end
    add_index :security_containers, :name, unique: true
  end
end
