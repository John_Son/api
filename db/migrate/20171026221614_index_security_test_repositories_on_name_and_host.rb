class IndexSecurityTestRepositoriesOnNameAndHost < ActiveRecord::Migration[5.0]
  # At the time this migration was written, there are very few records in the security_test_repositories
  # table on prod. But this may be needed for other systems and perhaps need to be run on prod at some point
  # in the future.
  disable_ddl_transaction!

  def up
    remove_index :security_test_repositories, :host

    add_index :security_test_repositories, :host, unique: true, algorithm: :concurrently
    add_index :security_test_repositories, :name, unique: true, algorithm: :concurrently
  end

  def down
    remove_index :security_test_repositories, :host
    remove_index :security_test_repositories, :name

    add_index :security_test_repositories, :host, algorithm: :concurrently
  end
end
