class CreateApiTokens < ActiveRecord::Migration[4.2]
  def change
    create_table :api_tokens do |t|
      t.string :value, null: false
      t.integer :state, null: false, default: 0
      t.references :user, index: true

      t.timestamps null: false
    end
    add_index :api_tokens, :value, unique: true
    add_foreign_key :api_tokens, :users, on_delete: :cascade
  end
end
